﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;

public class OnDestroyTourist : NetworkBehaviour {

    public GameObject toInstantiate;

    [ClientRpc]
	public void RpcDestroyAlien()
    {
        Instantiate(toInstantiate,transform.position + new Vector3(0,1.5f,0),transform.rotation);
    }
}
