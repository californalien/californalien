﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using com.ootii.Messages;

public class ScannerTransformTo0 : MonoBehaviour {

    RectTransform trans;
    public bool useRectTransform;

    void Awake()
    {
        MessageDispatcher.AddListener(OotiEvent.ON_END_LEVEL_LOAD.ToString(), SetToOrigin);
    }

    void OnDestroy()
    {
        MessageDispatcher.RemoveListener(OotiEvent.ON_END_LEVEL_LOAD.ToString(), SetToOrigin);
    }

	// Use this for initialization
	void Start () {
        if(useRectTransform == true)
            trans = GetComponent<RectTransform>();
	}

    void SetToOrigin(IMessage mess)
    {
        if (useRectTransform == true)
        {
            trans.offsetMin = new Vector2(0, 0);
            trans.offsetMax = new Vector2(0, 0);
        }
        else
        {
            transform.position = Vector3.zero;
        }
    }
}
