﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections.Generic;
using System.Collections;

/*
    MonoBehaviour à ajouter a la scène pour initialiser la SLML
        -Spawn le prefab du joueur
        -Envoi des callback de l'etat reseau au listener
        -Initialise le LobbyNetworkManager 
*/

public class SLML : MonoBehaviour {

    //Signal envoyé pour identifier l'application sur le réseau
    public string identifier = "SLML";

    //Prefab du Player
    public SLML_Player playerPrefab;
    //Listener pour interpretation des Callbacks venant du networkManager
    public SLML_Listener listener;

    //Nombre joueurs minimum pour lancer le jeu
    public int minPlayers = 2;
    //Nombre joueurs maximum pour lancer le jeu
    public int maxPlayers = 4;
    //Temps avant de lancer le jeu lorsque tout les joueurs sont prets
    public float durationDelay = 3;

    public bool verboseLogging = false;
    public bool localhostDebug = false;
    public bool DebugGUI = true;

    //Network manager, gere le lobby et les connexions
    private SLML_Manager networkManager;

    void Awake()
    {
        ValidateConfig();

        networkManager = (Instantiate(Resources.Load("Prefabs/SLML_Manager")) as GameObject).GetComponent<SLML_Manager>();
        
        if(networkManager != null)
        {
            //nom du gameObject dans la scene
            networkManager.name = "SLML_Manager";
            networkManager.transform.parent = transform.parent;
            //Utilise pour le debugging mais DANGEREUX sur mobile
            networkManager.runInBackground = true;
             
            networkManager.identifier = identifier;
            networkManager.minPlayers = minPlayers;
            networkManager.maxPlayers = maxPlayers;
           
            //affiche le GUI de debug du lobby
            networkManager.showLobbyGUI = false;

            networkManager.lobbyPlayerPrefab = playerPrefab;
            if (playerPrefab)
            {
                networkManager.gamePlayerPrefab = playerPrefab.gameObject;
            }
            networkManager.listener = listener;
            networkManager.verboseLogging = verboseLogging;
            networkManager.localhostDebug = localhostDebug;

            // Optionally create Debug GUI
            if (DebugGUI)
            {
                networkManager.GetComponent<SLML_DebugGUI>().enabled = true;
            }
        }
        else
        {
            Debug.Log("Problème initialisation du SLML_Manager", this);
        }
        
    }

    public void Update()
    {
        if (networkManager == null)
        {
            networkManager = FindObjectOfType(typeof(SLML_Manager)) as SLML_Manager;
            networkManager.listener = listener;

            if (networkManager.verboseLogging)
            {
                Debug.Log("#SLML# !! RECONNECTING !!");
            }
        }
    }

    public void ValidateConfig()
    {
        if (identifier == null)
        {
            Debug.LogError("#SLML# Donnez un Identifier pour la decouverte sur le réseau", this);
        }
        if (playerPrefab == null)
        {
            Debug.LogError("#SLML# Donnez un Player prefab", this);
        }
        if (listener == null)
        {
            Debug.LogError("#SLML# Donnez un Listener pour interpreter les callbacks", this);
        }
    }

    public List<SLML_Player> Players()
    {
        return networkManager.LobbyPlayers();
    }

    public void AutoConnect()
    {
        networkManager.minPlayers = minPlayers;
        networkManager.AutoConnect();
    }

    public void StartHosting()
    {
        networkManager.minPlayers = minPlayers;
        networkManager.StartHosting();
    }

    public void StartJoining()
    {
        networkManager.minPlayers = minPlayers;
        networkManager.StartJoining();
    }

    public void Cancel()
    {
        networkManager.Cancel();
    }

    public bool AreAllPlayersReady()
    {
        return networkManager.AreAllPlayersReady();
    }

    public float CountdownTimer()
    {
        return networkManager.allReadyCountdown;
    }

    public void StartLocalGameForDebugging()
    {
        networkManager.minPlayers = 1;
        networkManager.StartLocalGameForDebugging();
    }

    
}
