﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections.Generic;

public class SLML_Listener : MonoBehaviour
{
    [HideInInspector]
    public SLML mess;
    
    public void Awake()
    {
        mess = FindObjectOfType(typeof(SLML)) as SLML;
    }

    public virtual void OnStartConnecting()
    {
        // Override
    }

    public virtual void OnStopConnecting()
    {
        // Override
    }

    public virtual void OnReceivedBroadcast(string fromAddress, string data)
    {
        // Override
    }


    public virtual void OnDiscoveredServer(SLML_DiscoveredServer aServer)
    {
        // Override
    }
   

    public virtual void OnJoinedLobby()
    {
        // Override
    }

    public virtual void OnLeftLobby()
    {
        // Override
    }

    public virtual void OnCountdownStarted()
    {
        // Override
    }

    public virtual void OnCountdownCancelled()
    {
        // Override
    }

    public virtual void OnStartGame(List<SLML_Player> startingPlayers)
    {
        // Override
    }

    public virtual void OnAbortGame()
    {
        // Override
    }
}
