﻿using System;
using UnityEngine;
using UnityEngine.Networking;

//donnée reçue à propos des serveurs
[System.Serializable]
public class SLML_Data
{
    public int version = 1;
    public string peerId;
    public bool isOpen;
    public int numPlayers;

    public override string ToString()
    {
        return String.Format("{0}:{1}:{2}:{3}", version, peerId, isOpen ? 1 : 0, numPlayers);
    }

    public void FromString(string aString)
    {
        var items = aString.Split(':');
        version = Convert.ToInt32(items[0]);
        peerId = items[1];
        isOpen = (Convert.ToInt32(items[2]) != 0);
        numPlayers = Convert.ToInt32(items[3]);
    }
}
