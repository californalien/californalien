﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.Networking.NetworkSystem;
using UnityEngine.SceneManagement;


public class SLML_Server : NetworkDiscovery
{
    public SLML_Manager networkManager;
    public SLML_Data dataObject;
    public bool isOpen { get { return dataObject.isOpen; } set { dataObject.isOpen = value; } }
    public int numPlayers { get { return dataObject.numPlayers; } set { dataObject.numPlayers = value; } }

    void Start()
    {
        showGUI = false;
        useNetworkManager = false;
    }

    public void Setup(SLML_Manager networkManager)
    {
        this.networkManager = networkManager;
        broadcastKey = networkManager.identifier.GetHashCode(); // Make sure broadcastKey matches client
        isOpen = false;
        numPlayers = 0;

        dataObject = new SLML_Data();
        dataObject.peerId = networkManager.peerId;
        UpdateData();
    }

    public void UpdateData()
    {
        broadcastData = dataObject.ToString();
    }

    public void Reset()
    {
        isOpen = false;
        numPlayers = 0;
        UpdateData();
    }

    public void RestartBroadcast()
    {
        if (running)
        {
            StopBroadcast();
        }

        // Delay briefly to let things settle down
        CancelInvoke("RestartBroadcastInternal");
        Invoke("RestartBroadcastInternal", 0.5f);
    }

    private void RestartBroadcastInternal()
    {
        UpdateData();

        if (networkManager.verboseLogging)
        {
            Debug.Log("#SLML# Restarting server with data: " + broadcastData);
        }

        // You can't update broadcastData while the server is running so I have to reinitialize and restart it
        // I think Unity is fixing this

        if (!Initialize())
        {
            Debug.LogError("#SLML# Le network port est inaccesible!");
        }
        if (!StartAsServer())
        {
            Debug.LogError("#SLML# Impossible de broadcast!");
        }
    }
}
