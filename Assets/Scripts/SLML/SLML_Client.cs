﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.Networking.NetworkSystem;
using UnityEngine.SceneManagement;

public class SLML_Client : NetworkDiscovery
{
    public SLML_Manager networkManager;

    //Liste des serveurs decouverts
    public Dictionary<string, SLML_DiscoveredServer> discoveredServers;
    public const float ServerKeepAliveTime = 5.0f;
    public bool autoJoin;


    public Queue<string> receivedBroadcastLog;

    private const int maxLogLines = 4;
    private const string broadcastLogTokens = "-.";
    private int broadcastLogCounter = 0;

    void Start()
    {
        discoveredServers = new Dictionary<string, SLML_DiscoveredServer>();
        receivedBroadcastLog = new Queue<string>();
        showGUI = false;

        InvokeRepeating("CleanServerList", 3, 1);
    }

    public void Setup(SLML_Manager networkManager)
    {
        this.networkManager = networkManager;
        broadcastKey = networkManager.identifier.GetHashCode(); // Make sure broadcastKey matches server
    }

    public void Reset()
    {
        discoveredServers.Clear();
        receivedBroadcastLog.Clear();
        autoJoin = false;
    }

    public void StartJoining()
    {
        Reset();
        if (!Initialize())
        {
            Debug.LogError("#SLML# Network port is unavailable!");
        }
        if (!StartAsClient())
        {
            Debug.LogError("#SLML# Unable to listen!");
        }
        autoJoin = true;
    }

    public void CleanServerList()
    {
        var toRemove = new List<string>();
        foreach (var kvp in discoveredServers)
        {
            float timeSinceLastBroadcast = Time.time - kvp.Value.timestamp;
            if (timeSinceLastBroadcast > ServerKeepAliveTime)
            {
                toRemove.Add(kvp.Key);
            }
        }

        foreach (string server in toRemove)
        {
            discoveredServers.Remove(server);
        }
    }

    public override void OnReceivedBroadcast(string fromAddress, string rawData)
    {
        SLML_Data data = new SLML_Data();
        data.FromString(rawData);

        // DEBUG LOG
        broadcastLogCounter += 1;
        receivedBroadcastLog.Enqueue(broadcastLogTokens[broadcastLogCounter % broadcastLogTokens.Length] + " " + rawData);
        if (receivedBroadcastLog.Count > maxLogLines)
        {
            receivedBroadcastLog.Dequeue();
        }

        var server = new SLML_DiscoveredServer(data);
        server.rawData = rawData;
        server.ipAddress = fromAddress;
        server.timestamp = Time.time;

        bool newData = false;
        if (!discoveredServers.ContainsKey(fromAddress))
        {
            // New server
            discoveredServers.Add(fromAddress, server);
            newData = true;
        }
        else
        {
            if (discoveredServers[fromAddress].rawData != rawData)
            {
                // Old server with new info
                discoveredServers[fromAddress] = server;
                newData = true;
            }
            else
            {
                // Just update the timestamp
                discoveredServers[fromAddress].timestamp = Time.time;
                newData = false;
            }
        }

        networkManager.OnReceivedBroadcast(fromAddress, rawData);

        if (newData)
        {
            networkManager.OnDiscoveredServer(server);
        }
    }
}
