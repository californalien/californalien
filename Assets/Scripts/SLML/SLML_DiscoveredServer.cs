﻿using UnityEngine;
using System.Collections;

public class SLML_DiscoveredServer : SLML_Data
{
    public string rawData;
    public string ipAddress;
    public float timestamp;

    public SLML_DiscoveredServer(SLML_Data data)
    {
        version = data.version;
        peerId = data.peerId;
        isOpen = data.isOpen;
        numPlayers = data.numPlayers;
    }
}
