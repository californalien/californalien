﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.Networking.NetworkSystem;
using UnityEngine.SceneManagement;
using com.ootii.Messages;

public class SLML_Manager : NetworkLobbyManager
{

    public static SLML_Manager Instance;

    [Space]
    [Space]
    [Header("SLML")]
    //Signal envoyé pour identifier l'application sur le réseau
    public string identifier = "SLML";

    //Id de l'appareil
    public string deviceId;
    //Id pour le pairing, sous partie de l'id de l'appareil
    public string peerId;

    //Combien de temps chercher un serveur avant d'en demarrer un
    public float startHostingDelay = 2;
    //Temps avant de lancer le jeu lorsque tout les joueurs sont pret
    public float allReadyCountdownDuration = 0; 

    //Serveur
    public SLML_Server discoveryServer;
    //Client
    public SLML_Client discoveryClient;
    public SLML_Listener listener;

    public float allReadyCountdown = 0;

    private string BecomeHostAfterTime;
    private bool gameHasStarted = false;
    private bool joinedLobby = false;

    public bool verboseLogging = false;
    public bool localhostDebug = false;

    protected SLML_LobbyHook lobbyHook;


    string GenerateHash()
    {
        string hash = "";

        for(int i = 0; i<32; i++)
        {
           hash+= UnityEngine.Random.Range(0, 9);
        }

        return Md5Sum(hash);
    }

    public string Md5Sum(string strToEncrypt)
    {
        System.Text.UTF8Encoding ue = new System.Text.UTF8Encoding();
        byte[] bytes = ue.GetBytes(strToEncrypt);

        // encrypt bytes
        System.Security.Cryptography.MD5CryptoServiceProvider md5 = new System.Security.Cryptography.MD5CryptoServiceProvider();
        byte[] hashBytes = md5.ComputeHash(bytes);

        // Convert the encrypted bytes back to a string (base 16)
        string hashString = "";

        for (int i = 0; i < hashBytes.Length; i++)
        {
            hashString += System.Convert.ToString(hashBytes[i], 16).PadLeft(2, '0');
        }

        return hashString.PadLeft(32, '0');
    }

    public virtual void Start()
    {
        Instance = this;

        lobbyHook = GetComponent<SLML_LobbyHook>();

        deviceId = GenerateHash(); // SystemInfo.deviceUniqueIdentifier;
        peerId = deviceId.Substring(0, 8);

        runInBackground = false;

        discoveryServer.Setup(this);
        discoveryClient.Setup(this);

        if (singleton != this)
        {
            Debug.LogWarning("#SLML# DUPLICATE SLML_MANAGER!");
            Destroy(gameObject);
        }

        Debug.Log(String.Format("#SLML# Initialized peer {0}, \'{1}\', {2}-{3} players",
            peerId, identifier, minPlayers, maxPlayers));
    }

    public void StartHosting()
    {
        if (verboseLogging)
        {
            Debug.Log("#SLML# StartHosting");
        }

        // Stop the broadcast server so we can start a regular hosting server
        StopServer();

        // Delay briefly to let things settle down
        CancelInvoke("StartHostingInternal");
        Invoke("StartHostingInternal", 0.5f);

        discoveryServer.isOpen = true;
        discoveryServer.RestartBroadcast();
    }

    private void StartHostingInternal()
    {
        if (StartHost() == null)
        {
            Debug.LogError("#SLML# Failed to start hosting!");
        }
    }

    public void StartLocalGameForDebugging()
    {
        if (StartHost() == null)
        {
            Debug.LogError("#SLML# Failed to start hosting!");
        }
    }

    public void StartBroadcasting()
    {
        if (!isNetworkActive)
        {
            // Must also start network server so the broadcast is sent properly
            if (!StartServer())
            {
                Debug.LogError("#SLML# Failed to start broadcasting!");
            }
        }

        discoveryServer.isOpen = false;
        discoveryServer.RestartBroadcast();
    }

    public void StartJoining()
    {
        discoveryClient.StartJoining();

        SendStartConnectingMessage();
    }

    public void AutoConnect()
    {
        StartBroadcasting();
        StartJoining();

        // Start hosting if we don't find anything for a while...
        BecomeHostAfterTime = "MaybeStartHosting";
        Invoke(BecomeHostAfterTime, startHostingDelay);
    }

    public void Cancel()
    {
        if (verboseLogging)
        {
            Debug.Log("#SLML# Cancelling!");
        }

        if (gameHasStarted)
        {
            SendAbortGameMessage();
            gameHasStarted = false;
            Invoke("Cancel", 0.1f);
            return;
        }

        CancelInvoke(BecomeHostAfterTime);

        if (discoveryClient.hostId != -1)
        {
            discoveryClient.StopBroadcast();
        }
        discoveryClient.Reset();

        if (discoveryServer.hostId != -1)
        {
            discoveryServer.StopBroadcast();
        }
        discoveryServer.Reset();

        StopClient();
        StopServer();

        NetworkServer.Reset();
    }

    public void OnReceivedBroadcast(string aFromAddress, string aData)
    {
        SendReceivedBroadcastMessage(aFromAddress, aData);
    }

    public void OnDiscoveredServer(SLML_DiscoveredServer aServer)
    {
        if (verboseLogging)
        {
            Debug.Log("#SLML# Discovered " + aServer.rawData);
        }

        SendDiscoveredServerMessage(aServer);

        bool shouldJoin = false;
        bool isMe = (aServer.peerId == peerId);
        if (!isMe || localhostDebug)
        {
            if (aServer.isOpen && aServer.numPlayers < maxPlayers)
            {
                if (aServer.numPlayers > 0)
                {
                    shouldJoin = true; // Pick the first server that already has players
                }
                else if (BestHostingCandidate() == aServer.peerId)
                {
                    shouldJoin = true;
                }
            }
        }

        if (shouldJoin)
        {
            if (verboseLogging)
            {
                Debug.Log("#SLML# Should join!");
            }

            // We found something! Cancel hosting...
            CancelInvoke(BecomeHostAfterTime);

            if (client == null)
            {
                if (discoveryClient.autoJoin)
                {
                    JoinServer(aServer.ipAddress, networkPort);
                }
                else
                {
                    if (verboseLogging)
                    {
                        Debug.Log("#SLML# JOIN CANCELED: Auto join disabled.");
                    }
                }
            }
            else
            {
                if (verboseLogging)
                {
                    Debug.Log("#SLML# JOIN CANCELED: Already have client.");
                }
            }
        }
        else
        {
            if (verboseLogging)
            {
                Debug.Log("#SLML# Should NOT join.");
            }
        }
    }

    void MaybeStartHosting()
    {
        // If I'm the best candidate, start hosting!
        int numCandidates = GetHostingCandidates().Count;
        bool enoughPlayers = (numCandidates >= minPlayers);

        if (verboseLogging)
        {
            Debug.Log("#SLML# MaybeStartHosting? Found " + numCandidates + "/" + minPlayers + " candidates");
        }

        if (enoughPlayers && BestHostingCandidate() == peerId)
        {
            StartHosting();
        }
        else
        {
            // Wait, then try again...
            Invoke(BecomeHostAfterTime, startHostingDelay);
        }
    }

    List<string> GetHostingCandidates()
    {
        var candidates = new List<string>();

        // Grab server peer IDs
        foreach (SLML_DiscoveredServer server in discoveryClient.discoveredServers.Values)
        {
            if (server.numPlayers < 2 && !server.isOpen)
            {
                candidates.Add(server.peerId);
            }
        }

        if (verboseLogging)
        {
            Debug.Log("#SLML# Hosting candidates: " + String.Join(",", candidates.ToArray()));
        }
        return candidates;
    }

    string BestHostingCandidate()
    {
        var allCandidates = GetHostingCandidates();
        if (allCandidates.Count < minPlayers)
        {
            return "";
        }

        // Pick lowest peer ID as server (maybe use fastest/best device instead?)
        allCandidates.Sort();
        string bestCandidate = allCandidates[0];

        if (verboseLogging)
        {
            Debug.Log("#SLML# Picked " + bestCandidate + " as best candidate");
        }
        return bestCandidate;
    }

    void JoinServer(string aAddress, int aPort)
    {
        if (verboseLogging)
        {
            Debug.Log("#SLML# Joining " + aAddress + " : " + aPort);
        }

        // Stop being a server
        StopHost();
        networkAddress = aAddress;
        networkPort = aPort;

        // Delay briefly to let things settle down
        CancelInvoke("JoinServerInternal");
        Invoke("JoinServerInternal", 0.5f);
    }

    private void JoinServerInternal()
    {
        StartClient();
    }

    public bool AreAllPlayersReady()
    {
        return (NumReadyPlayers() == NumPlayers());
    }

    public List<SLML_Player> LobbyPlayers()
    {
        var lobbyPlayers = new List<SLML_Player>();
        foreach (var player in lobbySlots)
        {
            if (player != null)
            {
                lobbyPlayers.Add(player as SLML_Player);
            }
        }
        return lobbyPlayers;
    }

    public int NumReadyPlayers()
    {
        int readyCount = 0;
        foreach (var player in LobbyPlayers())
        {
            if (player.readyToBegin)
            {
                readyCount += 1;
            }
        }
        return readyCount;
    }

    public int NumPlayers()
    {
        return LobbyPlayers().Count;
    }

    public bool IsHost()
    {
        NetworkIdentity networkObject = FindObjectOfType(typeof(NetworkIdentity)) as NetworkIdentity;
        return (networkObject != null && networkObject.isServer && IsClientConnected() && NumPlayers() >= minPlayers);
    }

    public void Update()
    {

    }

    public bool IsBroadcasting()
    {
        return discoveryServer.running;
    }

    public bool IsJoining()
    {
        return discoveryClient.running;
    }

    public bool IsConnected()
    {
        return IsClientConnected();
    }

    // ------------------------ lobby server virtuals ------------------------

    public override bool OnLobbyServerSceneLoadedForPlayer(GameObject lobbyPlayer, GameObject gamePlayer)
    {
        //This hook allows you to apply state data from the lobby-player to the game-player
        //just subclass "LobbyHook" and add it to the lobby object.
        Debug.Log("LobbyHook " + gamePlayer.name);
        if (lobbyHook)
            lobbyHook.OnLobbyServerSceneLoadedForPlayer(this, lobbyPlayer, gamePlayer);

        return true;
    }


    public override void OnLobbyClientSceneChanged(NetworkConnection conn)
    {
        base.OnLobbyClientSceneChanged(conn);

        if(SceneManager.GetActiveScene().name == playScene)
        {
            MessageDispatcher.SendMessage("ON_START_GAME");
        }
    }

    public override void OnLobbyServerConnect(NetworkConnection conn)
    {
        if (verboseLogging)
        {
            Debug.Log("#SLML# OnLobbyServerConnect (num players = " + NumPlayers() + ")");
        }

        // If we've reached max players, stop the broadcast
        if (NumPlayers() + 1 >= maxPlayers)
        {
            if (verboseLogging)
            {
                Debug.Log("#SLML# Max players reached, stopping broadcast");
            }

            discoveryServer.StopBroadcast();
        }
        else
        {
            // Update player count for broadcast
            discoveryServer.numPlayers = NumPlayers() + 1;
            discoveryServer.RestartBroadcast();
        }
    }

    public override void OnLobbyServerDisconnect(NetworkConnection conn)
    {
        if (verboseLogging)
        {
            Debug.Log("#SLML# OnLobbyServerDisconnect (num players = " + NumPlayers() + ")");
        }

        if (gameHasStarted)
        {
            SendAbortGameMessage();
            gameHasStarted = false;

            Cancel();
        }
        else
        {
            // If we're below the minimum required players, close the lobby
            if (NumPlayers() < minPlayers)
            {
                if (verboseLogging)
                {
                    Debug.Log("#SLML# Not enough players, cancelling game");
                }
                Cancel();
            }
            else
            {
                if (allReadyCountdown > 0)
                {
                    // Cancel the countdown
                    allReadyCountdown = 0;
                    SendCountdownCancelledMessage();
                }

                // Update player count for broadcast
                discoveryServer.numPlayers = NumPlayers();
                discoveryServer.RestartBroadcast();
            }
        }
    }

    public override GameObject OnLobbyServerCreateLobbyPlayer(NetworkConnection conn, short playerControllerId)
    {
        if (verboseLogging)
        {
            Debug.Log("#SLML# OnLobbyServerCreateLobbyPlayer (num players " + NumPlayers() + ")");
        }

        GameObject newLobbyPlayer = Instantiate(lobbyPlayerPrefab.gameObject, Vector3.zero, Quaternion.identity) as GameObject;
        return newLobbyPlayer;
    }

    public override void OnLobbyServerPlayersReady()
    {
        if (verboseLogging)
        {
            Debug.Log("#SLML# OnLobbyServerPlayersReady (num players " + NumPlayers() + ")");
        }

        if (AreAllPlayersReady())
        {

            if (allReadyCountdownDuration > 0)
            {
                // Start all ready countdown
                allReadyCountdown = allReadyCountdownDuration;
                SendCountdownStartedMessage();
            }
            else
            {
                //c'est toto qui rentre dans un café
                //...
                //ça fait PLOUF!!!

                // Start game not really immediately <= wait for an animation to play.. gross
                //ServerChangeScene(playScene);
                HCPlayerScript.instance.RpcGoNextScene();
                Invoke("GoNextScene", 2.5f);


            }
        }
    }

    // ------------------------ lobby client virtuals ------------------------

    public override void OnLobbyClientEnter()
    {
        if (verboseLogging)
        {
            Debug.Log("#SLML# OnLobbyClientEnter " + listener);
        }

        // Stop listening for other servers
        if (discoveryClient.running)
        {
            discoveryClient.StopBroadcast();
        }

        // Stop broadcasting as a server
        if (discoveryServer.running)
        {
            discoveryServer.StopBroadcast();
        }

        SendJoinedLobbyMessage();

        joinedLobby = true;
    }

    public override void OnLobbyClientExit()
    {
        if (verboseLogging)
        {
            Debug.Log("#SLML# OnLobbyClientExit (num players = " + numPlayers + ")");
        }

        // Check to see if we've actually joined a lobby
        if (joinedLobby)
        {
            SendLeftLobbyMessage();
        }
        else
        {
            SendStopConnectingMessage();
        }
        joinedLobby = false;
    }

    ////////////////////////////////////////////////////////////////////////////////
    //
    //  API messages
    //

    public void SendStartConnectingMessage()
    {
        listener.OnStartConnecting();
    }

    public void SendStopConnectingMessage()
    {
        listener.OnStopConnecting();
    }

    public void SendReceivedBroadcastMessage(string fromAddress, string data)
    {
        listener.OnReceivedBroadcast(fromAddress, data);
    }

    public void SendDiscoveredServerMessage(SLML_DiscoveredServer server)
    {
        listener.OnDiscoveredServer(server);
    }

    public void SendJoinedLobbyMessage()
    {
        listener.OnJoinedLobby();
    }

    public void SendLeftLobbyMessage()
    {
        listener.OnLeftLobby();
    }

    public void SendCountdownStartedMessage()
    {
        listener.OnCountdownStarted();
    }

    public void SendCountdownCancelledMessage()
    {
        listener.OnCountdownCancelled();
    }

    public void SendStartGameMessage(List<SLML_Player> startingPlayers)
    {
        listener.OnStartGame(startingPlayers);
    }

    public void SendAbortGameMessage()
    {
        listener.OnAbortGame();
    }

    ///JE SUIS PAS CONTENT DE METTRE CA ICI MAIS BON....
    ///
    ///
    ///

    public Animator ator;

    public void GoNextScene()
    {
        ServerChangeScene(playScene);
        gameHasStarted = true;
        SendStartGameMessage(LobbyPlayers());
    }

    
}
