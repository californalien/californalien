﻿using UnityEngine;
using com.ootii.Messages;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.Networking;
using System.Collections;

public class MyLobbyManager : MonoBehaviour {

    
    [Header("Debug")]
    [Space]
    public bool verbose;
    public bool debugConnection;

    [Space]
    [Header("UI Reference")]

    public RectTransform lobbyStart;
    public RectTransform lobbyConnecting;
    public RectTransform lobbyready;

    protected RectTransform currentPanel;

    void Start()
    {
        MessageDispatcher.AddListener("ON_START",AskForConnection);
        MessageDispatcher.AddListener("ON_CONNECTING", BeginSearch);
        MessageDispatcher.AddListener("ON_LEFT_LOBBY", ReturnToMenu);
        MessageDispatcher.AddListener("ON_STOP_CONNECTING", ReturnToMenu);
        MessageDispatcher.AddListener("ON_JOINED_LOBBY", BeginWaiting);
        MessageDispatcher.AddListener("ON_START_GAME", SwitchToGameUI);

        ChangeTo(lobbyStart);
    }

    void OnDestroy()
    {
        MessageDispatcher.RemoveListener("ON_START", AskForConnection);
        MessageDispatcher.RemoveListener("ON_CONNECTING", BeginSearch);
        MessageDispatcher.RemoveListener("ON_LEFT_LOBBY", ReturnToMenu);
        MessageDispatcher.RemoveListener("ON_STOP_CONNECTING", ReturnToMenu);
        MessageDispatcher.RemoveListener("ON_JOINED_LOBBY", BeginWaiting);
        MessageDispatcher.RemoveListener("ON_START_GAME", SwitchToGameUI);
    }

    //Switch le panel Actif
    public void ChangeTo(RectTransform newPanel)
    {
        if (currentPanel != null)
        {
            currentPanel.gameObject.SetActive(false);
        }

        if (newPanel != null)
        {
            newPanel.gameObject.SetActive(true);
        }

        currentPanel = newPanel;
    }

    //****************************
    //*******CALLBACKS OOTI*******
    //****************************
    void AskForConnection(IMessage mess)
    {
        if (FormationInfo.haveMadeFormation == false)
        {
            MessageDispatcher.SendMessage("FORMATION_ALERT");
            return;
        }

        if(debugConnection)
        {
            SLML_Manager.Instance.gameObject.GetComponent<SLML_DebugGUI>().enabled = true;
        }
        else
            SLML_Manager.Instance.AutoConnect();
    }

    void BeginSearch(IMessage mess)
    {
        ChangeTo(lobbyConnecting);
    }

    void ReturnToMenu(IMessage mess)
    {
        if (verbose)
            Debug.Log("#LobbyManager# ON_STOP_CONNECTING");
        ChangeTo(lobbyStart);
    }

    void BeginWaiting(IMessage mess)
    {
        ChangeTo(lobbyready);
    }

    void SwitchToGameUI(IMessage mess)
    {
        Debug.Log("Switch");
        ChangeTo(null);
    }
}
