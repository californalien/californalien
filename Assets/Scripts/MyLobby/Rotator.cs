﻿using UnityEngine;
using System.Collections;

public class Rotator : MonoBehaviour {

    public float zSpeed;
    bool isRotating = false;
    public bool startRot;

	// Use this for initialization
	void Start () {

        if(startRot)
            isRotating = true;

    }
	
	// Update is called once per frame
	void Update ()
    {
        if (isRotating)
        {
            transform.Rotate(new Vector3(0, 0, zSpeed * Time.deltaTime));
        }
	}

    public void MakeRotate()
    {
        isRotating = true;
    }

    public void StopRotate()
    {
        isRotating = false;
        transform.rotation = Quaternion.identity;
    }
}
