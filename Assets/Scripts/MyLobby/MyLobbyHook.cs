﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class MyLobbyHook : SLML_LobbyHook {

    public override void OnLobbyServerSceneLoadedForPlayer(NetworkManager manager, GameObject lobbyPlayer, GameObject gamePlayer)
    {
        base.OnLobbyServerSceneLoadedForPlayer(manager, lobbyPlayer, gamePlayer);

        HCPlayerScript lobby = lobbyPlayer.GetComponent<HCPlayerScript>();
        HC_InGamePlayer player = gamePlayer.GetComponent<HC_InGamePlayer>();

        player.name = lobby.name;
        player.playerColor = lobby.playerColor;
    }
}
