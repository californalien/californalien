﻿using UnityEngine;
using System.Collections.Generic;

[CreateAssetMenu(fileName = "CupritData", menuName = "Alien/ProcGen/CulpritData", order = 3)]
public class CulpritsData : ScriptableObject
{
    public AlienData alienCulprit;
    public List<AlienData> alienClones;
}
