﻿using UnityEngine;
using System.Collections.Generic;

[CreateAssetMenu(fileName = "AlienMap", menuName = "Alien/CharaMap/AlienMap", order = 1)]
public class AlienMap : ScriptableObject
{
    public Sprite head;
    public Sprite eyes;
    public Sprite mouth;
    public Sprite eyebrows;
    public Sprite body;

    public Sprite leftUpperArm;
    public Sprite leftLowerArm;
    public Sprite leftHand;

    public Sprite rightUpperArm;
    public Sprite rightLowerArm;
    public Sprite rightHand;

    public Sprite leftUpperLeg;
    public Sprite leftLowerLeg;

    public Sprite rightUpperLeg;
    public Sprite rightLowerLeg;

}
