﻿using UnityEngine;
using System.Collections.Generic;

[CreateAssetMenu(fileName = "AlienStoryDataBase", menuName = "Alien/Story/AlienStoryDataBase", order = 5)]
public class AlienStoryDataBase : ScriptableObject
{
    public List<AlienStory> stories;
}