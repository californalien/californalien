﻿using UnityEngine;
using System.Collections.Generic;

[CreateAssetMenu(fileName = "AlienVerifier", menuName = "Alien/ProcGen/AlienVerifier", order = 2)]
public class AlienVerifier : ScriptableObject
{
    public List<AlienData> culprits;

    public List<AlienData> suspects;

}
