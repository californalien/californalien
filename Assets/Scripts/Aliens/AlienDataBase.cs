﻿using UnityEngine;
using System.Collections.Generic;

[CreateAssetMenu(fileName = "AlienDataBase", menuName = "Alien/ProcGen/AlienDataBase", order = 4)]
public class AlienDataBase : ScriptableObject
{
    public List<CulpritsData> culprits;
    public List<AlienData> randoms;
}
