﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections.Generic;
using Spriter2UnityDX;

public class CharacterInitializer : NetworkBehaviour {

    [SyncVar]
    string hash;

    public SpriteRenderer head;
    public SpriteRenderer eyes;
    public SpriteRenderer eyebrows;
    public SpriteRenderer mouth;
    public SpriteRenderer body;

    public SpriteRenderer leftUpperArm;
    public SpriteRenderer leftLowerArm;
    public SpriteRenderer leftHand;

    public SpriteRenderer rightUpperArm;
    public SpriteRenderer rightLowerArm;
    public SpriteRenderer rightHand;

    public SpriteRenderer leftUpperLeg;
    public SpriteRenderer leftLowerLeg;

    public SpriteRenderer rightUpperLeg;
    public SpriteRenderer rightLowerLeg;

    // Use this for initialization
    private void Start()
    {
        string[] carac = hash.Split('/');

        foreach (string s in carac)
        {
            PushCharacterMap(s);
        }
    }
	

    private void PushCharacterMap(string charaMapName)
    {
        AlienMap map = (AlienMap)Resources.Load("Data/Aliens/AlienMap/Aliens/" + charaMapName);
        //bouhhhh c'est trop caca ça...
        //mais pas le temps de faire beaucoup mieux..;

        if (map.head != null && head != null)
        {
            head.sprite = map.head;
            head.gameObject.GetComponent<StencilHandler>().SetLayer(charaMapName);
        }

        if (map.eyes != null && eyes != null)
        {
            /*for (int i = 0; i < map.eyes.Count; i++)
                eyes.Sprites[i] = map.eyes[i];*/

            eyes.sprite = map.eyes;
            eyes.gameObject.gameObject.GetComponent<StencilHandler>().SetLayer(charaMapName);
        }

        if (map.eyebrows != null && eyebrows != null)
        {
            eyebrows.sprite = map.eyebrows;
            eyebrows.gameObject.GetComponent<StencilHandler>().SetLayer(charaMapName);
        }

        if (map.mouth != null && mouth != null)
        {
            /*for (int i = 0; i < map.mouth.Count; i++)
                mouth.Sprites[i] = map.mouth[i];*/
            mouth.sprite = map.mouth;
            mouth.gameObject.GetComponent<StencilHandler>().SetLayer(charaMapName);

        }

        if (map.body != null && body != null)
        {
            body.sprite = map.body;
            body.gameObject.GetComponent<StencilHandler>().SetLayer(charaMapName);

        }

        if (map.leftUpperArm != null && leftUpperArm != null)
        {
            leftUpperArm.sprite = map.leftUpperArm;
            leftUpperArm.gameObject.GetComponent<StencilHandler>().SetLayer(charaMapName);

        }

        if (map.leftLowerArm != null && leftLowerArm != null)
        {
            leftLowerArm.sprite = map.leftLowerArm;
            leftLowerArm.gameObject.GetComponent<StencilHandler>().SetLayer(charaMapName);

        }

        if (map.leftHand != null && leftHand != null)
        {
            /*for (int i = 0; i < map.leftHand.Count; i++)
                leftHand.Sprites[i] = map.leftHand[i];*/

            leftHand.sprite = map.leftHand;
            leftHand.gameObject.GetComponent<StencilHandler>().SetLayer(charaMapName);

        }

        if (map.rightUpperArm != null && rightUpperArm != null)
        {
            rightUpperArm.sprite = map.rightUpperArm;
            rightUpperArm.gameObject.GetComponent<StencilHandler>().SetLayer(charaMapName);

        }

        if (map.rightLowerArm != null && rightLowerArm != null)
        {
            rightLowerArm.sprite = map.rightLowerArm;
            rightLowerArm.gameObject.GetComponent<StencilHandler>().SetLayer(charaMapName);

        }

        if (map.rightHand != null && rightHand != null)
        {
            /*for (int i = 0; i < map.rightHand.Count; i++)
                rightHand.Sprites[i] = map.rightHand[i];*/

            rightHand.sprite = map.rightHand;
            rightHand.gameObject.GetComponent<StencilHandler>().SetLayer(charaMapName);

        }

        if (map.leftUpperLeg != null && leftUpperLeg != null)
        {
            leftUpperLeg.sprite = map.leftUpperLeg;
            leftUpperLeg.gameObject.GetComponent<StencilHandler>().SetLayer(charaMapName);

        }

        if (map.leftLowerLeg != null && leftLowerLeg != null)
        {
            leftLowerLeg.sprite = map.leftLowerLeg;
            leftLowerLeg.gameObject.GetComponent<StencilHandler>().SetLayer(charaMapName);

        }

        if (map.rightUpperLeg != null && rightUpperLeg != null)
        {
            rightUpperLeg.sprite = map.rightUpperLeg;
            rightUpperLeg.gameObject.GetComponent<StencilHandler>().SetLayer(charaMapName);

        }

        if (map.rightLowerLeg != null && rightLowerLeg != null)
        {
            rightLowerLeg.sprite = map.rightLowerLeg;
            rightLowerLeg.gameObject.GetComponent<StencilHandler>().SetLayer(charaMapName);

        }

    }
    public void GenerateAlien(string value)
    {
        hash = value;
        
    }
}
