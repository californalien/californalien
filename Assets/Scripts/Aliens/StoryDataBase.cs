﻿using UnityEngine;
using System.Collections.Generic;

public class StoryDataBase : ScriptableObject
{
    public List<AlienStory> stories;
}
