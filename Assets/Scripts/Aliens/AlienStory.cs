﻿using UnityEngine;
using System.Collections;

[CreateAssetMenu(fileName = "AlienStory", menuName = "Alien/Story/AlienStory", order = 5)]
public class AlienStory : ScriptableObject
{
    public string Name;
    [TextArea(3, 10)]
    public string Story;
}
