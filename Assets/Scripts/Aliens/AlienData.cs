﻿using UnityEngine;
using System.Collections.Generic;

[CreateAssetMenu(fileName = "AlienData", menuName = "Alien/ProcGen/AlienData", order = 1)]
public class AlienData : ScriptableObject
{
    public Layer Head;
    public Layer Arms;
    public Layer Body;
    public Layer Legs;

    public bool HaveAStory;

    public string Name;

    [TextArea(3, 10)]
    public string Story;

    public string Hash;

    public void OnValidate()
    {
        Hash = "Head_" + Head + "/" + "Arms_" + Arms + "/" + "Body_" + Body + "/" + "Legs_" + Legs;
    }
}

