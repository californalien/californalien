﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;
using com.ootii.Messages;

[RequireComponent(typeof(EventTrigger))]
public class EventSender : MonoBehaviour
{

    public bool verbose;
    public void SendEventMessage(string mess)
    {
        if (verbose)
            Debug.Log("#EventSender# " + mess + " sended");
        MessageDispatcher.SendMessage(mess);
    }
}