﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ButtonActivateObject : MonoBehaviour {

	private Image button;

	public Sprite buttonNormal, buttonClicked;
	public GameObject toActivate; 
	bool isCliked = false;

	void Start()
	{
		button = GetComponent<Image>();
		toActivate.SetActive(false);
	}

	public void OnClick()
	{
		if(isCliked == false)
		{
			button.sprite = buttonClicked;
			//toActivate.sprite = ledClicked;
			isCliked = true;
			toActivate.SetActive(true);
		}
		else
		{
			button.sprite = buttonNormal;
			//toActivate.sprite = ledNormal;
			isCliked = false;
			toActivate.SetActive(false);
		}
	}


}
