﻿using UnityEngine;
using System.Collections;

public class CamShakeSimple : MonoBehaviour
{
    public static CamShakeSimple instance;
    Vector3 originalCameraPosition;

    float shakeAmt = 0;

    GameObject mainCamera;

    void Start()
    {
        instance = this;
        mainCamera = gameObject;
        originalCameraPosition = mainCamera.transform.position;
    }

    public void MakeShake()
    {
        FMODUnity.RuntimeManager.PlayOneShot("event:/jindef", transform.position);

        /*shakeAmt = 0.4f;
        originalCameraPosition = transform.position;
        InvokeRepeating("CameraShake", 0, .01f);
        Invoke("StopShaking", 0.4f);*/
    }

    void CameraShake()
    {
        if (shakeAmt > 0)
        {
            Vector3 pp = mainCamera.transform.position;
            float quakeAmt = Random.value * shakeAmt * 2 - shakeAmt;

            pp.y += quakeAmt;

            quakeAmt = Random.value * shakeAmt * 2 - shakeAmt;
            
             // can also add to x and/or z
            pp.x += quakeAmt;
            mainCamera.transform.position = pp;
        }
    }

    void StopShaking()
    {
        CancelInvoke("CameraShake");
        mainCamera.transform.position = originalCameraPosition;

    }

}