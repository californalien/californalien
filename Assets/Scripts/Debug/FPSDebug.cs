﻿using UnityEngine;
using System.Collections;
using com.ootii.Messages;

public class FPSDebug : MonoBehaviour {

    float deltaTime = 0.0f;

    void Update()
    {
        deltaTime += (Time.deltaTime - deltaTime) * 0.1f;

        if (Input.GetKeyDown(KeyCode.O))
            MessageDispatcher.SendMessage("ON_LEVEL_LOAD");

        if (Input.GetKeyDown(KeyCode.P))
            MessageDispatcher.SendMessage("ON_END_LEVEL_LOAD");
    }

    void OnGUI()
    {
        int w = Screen.width, h = Screen.height;

        GUIStyle style = new GUIStyle();

        Rect rect = new Rect(200, 0, w, h * 2 / 100);
        style.alignment = TextAnchor.UpperLeft;
        style.fontSize = h * 2 / 100;
        style.normal.textColor = new Color(1.0f, 1.0f, 1.0f, 1.0f);
        float msec = deltaTime * 1000.0f;
        float fps = 1.0f / deltaTime;
        string text = string.Format("{0:0.0} ms ({1:0.} fps)", msec, fps);
        GUI.Label(rect, text, style);


        rect = new Rect(400, 0, w, h * 2 / 100);
        style.alignment = TextAnchor.UpperLeft;
        style.fontSize = h * 2 / 100;
        style.normal.textColor = new Color(1.0f, 1.0f, 1.0f, 1.0f);

        text = string.Format(SLML_Manager.Instance.peerId);
        GUI.Label(rect, text, style);

    }
}
