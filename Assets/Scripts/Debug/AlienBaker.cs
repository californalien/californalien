﻿using UnityEngine;
//using UnityEditor;
using System.Collections.Generic;
using System.Collections;

public class AlienBaker : MonoBehaviour {

    public List<GameObject> human;
    public List<GameObject> head;
    public List<GameObject> arms;
    public List<GameObject> body;
    public List<GameObject> legs;

    //[MenuItem("AlienGenerator/Generate")]
    static void Generate()
    {
        GameObject.FindObjectOfType<AlienBaker>().generateAlien();
    }

    public void generateAlien()
    {
        GameObject obj = new GameObject();
        obj.name = "Alien" + Mathf.Ceil(Random.Range(0,100));
        GameObject GO;

        GO = Instantiate(human[Random.Range(0, human.Count)], obj.transform.position, obj.transform.rotation) as GameObject;
        GO.transform.SetParent(obj.transform);
        GO.name = "Human";

        GO = Instantiate(head[Random.Range(0, head.Count)], obj.transform.position, obj.transform.rotation) as GameObject;
        GO.transform.SetParent(obj.transform);
        GO.name = "Head";

        GO = Instantiate(arms[Random.Range(0, arms.Count)], obj.transform.position, obj.transform.rotation) as GameObject;
        GO.transform.SetParent(obj.transform);
        GO.name = "Arms";

        GO = Instantiate(body[Random.Range(0, body.Count)], obj.transform.position, obj.transform.rotation) as GameObject;
        GO.transform.SetParent(obj.transform);
        GO.name = "Body";

        GO = Instantiate(legs[Random.Range(0, legs.Count)], obj.transform.position, obj.transform.rotation) as GameObject;
        GO.transform.SetParent(obj.transform);
        GO.name = "Legs";

    }
}
