﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;

public class DebugMove : NetworkBehaviour {

    public float minXPos;
    public float maxXPos;
    public float moveSpeed;

    [SyncVar (hook = "ComputeOrientation")]
    private Vector3 target;
    private bool haveTarget;

    private Animator ator;

	// Use this for initialization
	void Start () {
        target = transform.position;
        haveTarget = true;
        ator = GetComponent<Animator>();
    }
	
	// Update is called once per frame
	void Update ()
    {
        if(haveTarget)
            GoToTarget();
    }

    void GoToTarget()
    {
        ator.SetBool("Walking", true);
        if (target.x > transform.position.x)
            transform.position = new Vector3((transform.position.x + moveSpeed * Time.deltaTime),
                                             transform.position.y,
                                             transform.position.z);
        else if (target.x < transform.position.x)
            transform.position = new Vector3((transform.position.x - moveSpeed * Time.deltaTime),
                                 transform.position.y,
                                 transform.position.z);

        if (Mathf.Abs(target.x - transform.position.x) < 0.1f)
        {
            ator.SetBool("Walking", false);
            haveTarget = false;
            if (isServer)
            {
                
                Invoke("getNextTarget", Random.Range(5, 25));
            }

        }

    }

    void getNextTarget()
    {
        target = new Vector3(Random.Range(minXPos, maxXPos), 0, 0);
        
    }

    void ComputeOrientation(Vector3 vec)
    {
        
        target = vec;
        haveTarget = true;
        if (vec.x < transform.position.x)
        {
            if (transform.localScale.x > 0)
            {
                transform.localScale = new Vector3(transform.localScale.x * -1, transform.localScale.y, transform.localScale.z);

            }
        }
        else
            transform.localScale = new Vector3(Mathf.Abs(transform.localScale.x), transform.localScale.y, transform.localScale.z);

    }
}
