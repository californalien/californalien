﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;
using com.ootii.Messages;

public class DebugMarqueur : MonoBehaviour {

    public GameObject marqueur;
    bool marqueurActif = false;
    GameObject marqueurGo;
    GameController controller;

    void Start()
    {
        controller = GameController.instance;
    }

    public void OnMouseUpAsButton()
    {
        if (controller.isInAlert == false)
            SetMarqueur();
        else
        {
            controller.localPlayer.CmdTryToArrest(GetComponent<NetworkIdentity>().netId);
            MessageDispatcher.SendMessage(OotiEvent.UNACTIVATE_ALERT.ToString());
        }
            

        
    }

    public void SetMarqueur()
    {
        if(!marqueurActif)
        {
            marqueurGo = Instantiate(marqueur, transform.position, transform.rotation) as GameObject;
            marqueurGo.transform.SetParent(transform);
            marqueurActif = true;
            Debug.Log(Camera.main.transform.parent.position);
            FMODUnity.RuntimeManager.PlayOneShot("event:/tag", transform.position );

        }
        else
        {
            Destroy(marqueurGo);
            Debug.Log(Camera.main.transform.parent.position);
            FMODUnity.RuntimeManager.PlayOneShot("event:/untag", transform.position );
            marqueurActif = false;
        }
    }
	
}
