﻿using UnityEngine;
using System.Collections.Generic;

public class ScannerListHandler : MonoBehaviour {

    public static ScannerListHandler Instance;
    public ScannerList scannerList;

    private List<int> notToPick;

    private int idIndex = 0;

	// Use this for initialization
	void Awake () {
        Instance = this;
        notToPick = new List<int>();
	}
	
	public string GetNextRandomScanner()
    {
        /* int index;

        do
        {
            index = Random.Range(0, scannerList.scanners.Count);

        } while (notToPick.Contains(index));

        */
        if(!notToPick.Contains(idIndex))
        {
            string toReturn = scannerList.scanners[idIndex].scannerName;
            notToPick.Add(idIndex);
            idIndex++;
            return toReturn;
        }

        return "";
    }
}
