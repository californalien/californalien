﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;
using com.ootii.Messages;

public class TimerController : NetworkBehaviour {

    public static TimerController instance;
    public float timer;
    private bool isStarted;
    private bool alert;

	// Use this for initialization
	void Start ()
    {
        instance = this;
        if(isServer)
        MessageDispatcher.AddListener("ON_LEVEL_BEGIN", StartTimer);
    }

    void OnDestroy()
    {
        //if(isServer)
        MessageDispatcher.RemoveListener("ON_LEVEL_BEGIN", StartTimer);
    }

    public void StartTimer(IMessage message)
    {
        RpcStartTimerInternal();
    }

    [ClientRpc]
    void RpcStartTimerInternal()
    {
        isStarted = true;
        MessageDispatcher.SendMessage("ON_TIMER_START");
    }

    public void StopTimer()
    {
        RpcStopTimerInternal();
        alert = false;
    }

    [ClientRpc]
    void RpcStopTimerInternal()
    {
        isStarted = false;
        timer = 300f;
        MessageDispatcher.SendMessage("ON_TIMER_STOP");
    }

    [ClientRpc]
    void RpcTimerEnd()
    {
        MessageDispatcher.SendMessage("ON_TIMER_END");
    }

    [ClientRpc]
    public void RpcTimerMalus(float amount)
    {
        timer -= amount;
    }

    // Update is called once per frame
    void Update ()
    {
        if(isStarted)
        { 
            if (isStarted && timer > 0)
                timer -= Time.deltaTime;

            if(timer < 30f && alert == false)
            {
                FMODUnity.RuntimeManager.PlayOneShot("event:/alertetimer", transform.position);
                alert = true;
            }

            if (!isServer) return;

            if(timer < 0 )
            {
                StopTimer();
                //RpcTimerEnd();
            }
        }

    }
}
