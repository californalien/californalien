﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections.Generic;
using com.ootii.Messages;

public class GameController : NetworkBehaviour {

    public static GameController instance;

    public HC_InGamePlayer localPlayer;

    public bool isInAlert;

    public List<GameObject> toInstantiate;
    private int musicIndex = 1;

    [SyncVar]
    public bool secondChance = true;

    

	// Use this for initialization
	void Awake ()
    {
        instance = this;
        MessageDispatcher.AddListener("ON_TIMER_STOP", TimerEnd);
        MessageDispatcher.AddListener("ON_LEVEL_END", EndLevel);
    }

    void OnDestroy()
    {
        MessageDispatcher.RemoveListener("ON_TIMER_STOP", TimerEnd);
        MessageDispatcher.RemoveListener("ON_GAME_END_RETURN", EndGame);
        MessageDispatcher.RemoveListener("ON_LEVEL_END", EndLevel);
    }

    void Start()
    {
        if(isServer)
        {
            MessageDispatcher.AddListener("ON_GAME_END_RETURN", EndGame);

            foreach (GameObject Go in toInstantiate)
            {
                Instantiate(Go, Vector3.zero, Quaternion.identity);
            }
        }

    }

    void TimerEnd(IMessage mess)
    {
        if (secondChance)
        {
            secondChance = false;
            //chargement
            MessageDispatcher.SendMessage("ON_LEVEL_LOAD");
            //regenerer le niveau
            MessageDispatcher.SendMessage("ON_BEGIN_GENERATE", 2f);
        }
        else
        {
            MessageDispatcher.SendMessage("ON_GAME_END");
        }

    }

    void EndLevel(IMessage mess)
    {

        //chargement
        MessageDispatcher.SendMessage("ON_LEVEL_LOAD");
        //regenerer le niveau
        MessageDispatcher.SendMessage("ON_BEGIN_GENERATE",2f);

        TimerController.instance.timer = 300f;


    }

    void EndGame(IMessage message)
    {
        int actualRecord = PlayerPrefs.GetInt("Record");

        if(ProcWorld.instance.nbLevel > actualRecord)
            PlayerPrefs.SetInt("Record", ProcWorld.instance.nbLevel);

        PlayerPrefs.Save();

        if (isServer)
            SLML_Manager.Instance.Cancel();
    }

    public int GetMusic()
    {
        musicIndex++;
        return musicIndex - 1;
    }

    [ClientRpc]
    public void RpcGetANewContract()
    {
        localPlayer.CmdGetAContract();
    }

    [ClientRpc]
    public void RpcVerifyMyAlien(int contractId)
    {
        if (localPlayer.myContract == contractId)
        {
            localPlayer.CmdGetAContract();
            FMODUnity.RuntimeManager.PlayOneShot("event:/jinvic", transform.position);

        }
    }

    [ClientRpc]
    public void RpcNotifyEndLevel()
    {
        MessageDispatcher.SendMessage(OotiEvent.ON_LEVEL_END.ToString());
    }

}
