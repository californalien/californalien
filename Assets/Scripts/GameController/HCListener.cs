﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;
using com.ootii.Messages;



//HC listener permet de notifier l'application de changements importants sur le reseau
public class HCListener : SLML_Listener
{
	public enum NetworkState
	{
		Init,
		Offline,
		Connecting,
		Connected,
		Disrupted
	};

    public GameObject LobbyManager;

	[HideInInspector]
	public NetworkState networkState = NetworkState.Init;

	public void Start()
	{
		networkState = NetworkState.Offline;
	}

	public override void OnStartConnecting()
	{
        MessageDispatcher.SendMessage("ON_CONNECTING");
		networkState = NetworkState.Connecting;
	}

	public override void OnStopConnecting()
	{
        MessageDispatcher.SendMessage("ON_STOP_CONNECTING");
        networkState = NetworkState.Offline;
	}

	public override void OnJoinedLobby()
	{
        MessageDispatcher.SendMessage("ON_JOINED_LOBBY");
        networkState = NetworkState.Connected;
	}

	public override void OnLeftLobby()
	{
        MessageDispatcher.SendMessage("ON_LEFT_LOBBY");
        networkState = NetworkState.Offline;
	}

	public override void OnCountdownStarted()
	{
		
	}

	public override void OnCountdownCancelled()
	{
		
	}

	public override void OnStartGame(List<SLML_Player> startingPlayers)
	{

    }

	public override void OnAbortGame()
	{
		Debug.Log("ABORT!");
	}

	void Update()
	{
		//networkStateField.text = networkState.ToString();	
	}
}
