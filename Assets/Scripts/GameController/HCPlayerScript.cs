﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
using System;
using System.Collections;
using com.ootii.Messages;



//etat d'un joueur pour la session
public class HCPlayerScript : SLML_Player
{

    [SyncVar]
    public Color playerColor;

    [SyncVar(hook = "MAJSeed")]
    public int randomSeed;

    public float hueMin, hueMax, saturationMin, saturationMax;
    public static HCPlayerScript instance;

    void MAJSeed( int value)
    {
        UnityEngine.Random.seed = (int)value;
    }


    public override void OnStartLocalPlayer()
	{
        instance = this;
        //On ajoute les listeners
        MessageDispatcher.AddListener("ON_READY", SetReady);
        MessageDispatcher.AddListener("ON_STOP_READY", SetNotReady);

        base.OnStartLocalPlayer();

        // Envoi de la couleur du joueur
        playerColor = UnityEngine.Random.ColorHSV(hueMin, hueMax, saturationMin, saturationMax, 1,1);
		CmdSetCustomPlayerInfo(playerColor);
	}

	[Command]
	public void CmdSetCustomPlayerInfo(Color aColour)
	{
        playerColor = aColour;
        randomSeed = UnityEngine.Random.seed;
	}

	public override void OnClientEnterLobby()
	{
		base.OnClientEnterLobby();

		// Brief delay to let SyncVars propagate
		Invoke("ShowPlayer", 0.5f);
	}

	public override void OnClientReady(bool readyState)
	{
		if (readyState)
		{
            GetComponent<Rotator>().MakeRotate();
		}
		else
		{
            GetComponent<Rotator>().StopRotate();
        }
	}

	void ShowPlayer()
	{
        transform.SetParent(GameObject.FindGameObjectWithTag("LobbyContainer").transform, false);

        GetComponent<Image>().color = playerColor;

        OnClientReady(IsReady());
    }

	public override void Update()
	{
        //mise en place de la boucle de jeu
		base.Update();
	}

	[ClientRpc]
	public void RpcOnStartedGame()
	{
        MessageDispatcher.SendMessage("ON_START_GAME");
	}

    public void SetReady(IMessage mess)
    {
        SendReadyToBeginMessage();
    }

    public void SetNotReady(IMessage mess)
    {
        SendNotReadyToBeginMessage();
    }

    public void OnDestroy()
    {
        MessageDispatcher.RemoveListener("ON_READY", SetReady);
        MessageDispatcher.RemoveListener("ON_STOP_READY", SetNotReady);
    }

    [ClientRpc]
    public void RpcGoNextScene()
    {
        SLML_Manager.Instance.ator.SetTrigger("nextStep");

        Invoke("StopMusic", 1.5f);
    }

    void StopMusic()
    {
        SLML_Manager.Instance.GetComponent<MenuMusic>().StopMusic();
    }
}
