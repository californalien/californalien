﻿using UnityEngine;
using System.Collections;


public enum RoomSize
{
    Small = 1,
    Medium = 2,
    Large = 3
}
