﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections.Generic;

public class ProcPopulation : MonoBehaviour {

    private enum AlienType
    {
        Culprit,
        Suspect,
        Random
    }

    public AlienDataBase database;

    //private int nbCulprits;
    public int nbSuspects;
    public int nbPopulation;

    public GameObject alienBase;
    private ContractHandler contractHandler;


    private ProcWorld world;
    // Use this for initialization
    void Awake ()
    {
        world = GetComponent<ProcWorld>();
    }


    public void GeneratePopulation()
    {
        GenerateCulprits();
        GenerateRandom();
    }

    void GenerateCulprits()
    {
        //nbCulprits = contractHandler.nbContracts;
        GameObject culprits = new GameObject();
        culprits.transform.parent = transform;
        culprits.name = "culpritsPop";

        List<int> culpritsList = ContractHandler.instance.GetCulpritsList();

        int index = 0;
        for (int i = 0; i < culpritsList.Count; i++)
        {
            string hash = database.culprits[culpritsList[i]].alienCulprit.Hash;

            GameObject obj = GenerateAlien(hash, culprits.transform);
            GenerateSuspects(culpritsList[i]);
            obj.GetComponent<AlienController>().culpritIndex = culpritsList[i];
            ContractHandler.instance.AddCulpritsObject(obj);
        }
    }

    void GenerateSuspects(int index)
    {
        GameObject suspects = GameObject.Find("suspectsPop");

        if (suspects == null)
        {
            suspects = new GameObject();
            suspects.transform.parent = transform;
            suspects.name = "suspectsPop";
        }

        for (int i = 0; i < nbSuspects; i++)
        {
            GenerateAlien(GetRandomSuspect(index).Hash, suspects.transform);
        }
    }

    void GenerateRandom()
    {
        GameObject rand = new GameObject();
        rand.transform.parent = transform;
        rand.name = "randomPop";
        int index =0;

        for (int i = 0; i < nbPopulation; i++)
        {
            GenerateAlien(GetRandomAlien(AlienType.Random,out index).Hash, rand.transform);
        }
            
    }

    private GameObject GenerateAlien(string AlienHash, Transform parent)
    {
        world.GetARoomSpawner();
        GameObject obj = Instantiate(alienBase, world.GetARoomSpawner() + new Vector3(0, 0, -1), Quaternion.identity) as GameObject;
        obj.GetComponent<CharacterInitializer>().GenerateAlien(AlienHash);
        obj.GetComponent<HumanInitializer>().GenerateHuman((int)Random.Range(0, 32));
        obj.transform.parent = parent;
        NetworkServer.Spawn(obj);
        return obj;
    }

    private AlienData GetRandomAlien(AlienType type, out int index)
    {
        index = 0;
        AlienData toReturn = null;

        switch (type)
        {
            case AlienType.Culprit:
                    index = Random.Range(0, database.culprits.Count);
                    toReturn = database.culprits[index].alienCulprit;
                break;
            case AlienType.Random:
                    index = Random.Range(0, database.culprits.Count);
                    toReturn = database.randoms[index];
                break;
            default:
                break;
        }

        return toReturn;
    }

    private AlienData GetRandomSuspect(int culpritIndex)
    {
        int index = Random.Range(0, database.culprits[culpritIndex].alienClones.Count);
        return database.culprits[culpritIndex].alienClones[index];
    }
}
