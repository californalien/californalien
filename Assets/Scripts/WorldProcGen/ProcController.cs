﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections.Generic;
using com.ootii.Messages;

public class ProcController : NetworkBehaviour
{
    [HideInInspector]
    public ProcWorld world;
    [HideInInspector]
    public ProcPopulation population;
    [HideInInspector]
    public ProcDecoration decoration;

    public int randomAugmentation = 4;
    public int levelAugmentation = 1;

    public void Start()
    {
        MessageDispatcher.AddListener("ON_BEGIN_GENERATE", OnLoadLevel);
        world = GetComponent<ProcWorld>();
        population = GetComponent<ProcPopulation>();
        decoration = GetComponent<ProcDecoration>();
        GenerateHotel();
    }

    void OnDestroy()
    {
        MessageDispatcher.RemoveListener("ON_BEGIN_GENERATE", OnLoadLevel);
    }

    public void Update()
    {
        if (Input.GetKeyDown(KeyCode.A))
            DestroyWorld();
        if (Input.GetKeyDown(KeyCode.Z))
            RegenerateHotel();
    }

    public void GenerateHotel()
    {
        world.GenerateWorld();
        decoration.GenerateBottom(new Vector3(0, 0, 0), world.levelSize, "WallBottom");
        decoration.GenerateTop(new Vector3(0, 0, 0), world.nbLevel,world.levelSize, "WallTop");
        decoration.GenerateWallLeft(new Vector3(0, 0, 0), world.nbLevel, "WallLeft");
        decoration.GenerateWallRight(new Vector3(0, 0, 0), world.nbLevel, world.levelSize, "WallRight");
        population.GeneratePopulation();
    }

    public void RegenerateHotel()
    {
        DestroyWorld();

        if(world.nbLevel < 10)
            world.nbLevel += levelAugmentation;

        population.nbPopulation += randomAugmentation;

        Invoke("GenerateHotel", 0.2f);
    }

    public void DestroyWorld()
    {
        List<GameObject> children = new List<GameObject>();
        foreach (Transform child in transform) children.Add(child.gameObject);
        children.ForEach(child => Destroy(child));

        world.RemoveSpawners();
    }


    //ooti calbacks

    void OnLoadLevel(IMessage mess)
    {
        ContractHandler.instance.GetNextContractSet();
        RegenerateHotel();
        Invoke("InvokeGetContract",0.5f);
    }

    void InvokeGetContract()
    {
        GameController.instance.RpcGetANewContract();
    }

}
