﻿using UnityEngine;
using System.Collections.Generic;

public class Room : MonoBehaviour {

    public RoomSize roomSize;

    [HideInInspector]
    public GameObject DoorRight;
    [HideInInspector]
    public GameObject DoorLeft;

    public List<Vector3> spawnPositions;

    void Awake()
    {
        GenerateDoors();
    }

    void OnDrawGizmos()
    {
        Color oldCol = Gizmos.color;
        Gizmos.color = Color.red;
        foreach( Vector3 V in spawnPositions)
        {
            Vector3 local = -transform.InverseTransformPoint(V);
            Gizmos.DrawSphere(local, 0.5f);
        }
        Gizmos.color = oldCol;

        if(DoorRight != null)
        Gizmos.DrawSphere(DoorRight.transform.position, 0.5f);
        if(DoorLeft != null)
        Gizmos.DrawSphere(DoorLeft.transform.position, 0.5f);
    }

    void GenerateDoors()
    {
        Bounds bounds = GetComponentInChildren<SpriteRenderer>().bounds;
        float distWall = bounds.size.x / 2;
        //float middle = bounds.size.y / 2;

        DoorRight = new GameObject();
        DoorRight.name = "DoorRight";

        DoorRight.transform.position = gameObject.transform.position;
        DoorRight.transform.parent = gameObject.transform;

        DoorRight.transform.position += new Vector3(DoorRight.transform.localPosition.x + distWall, DoorRight.transform.localPosition.y, DoorRight.transform.localPosition.z);


        DoorLeft = new GameObject();
        DoorLeft.name = "DoorLeft";

        DoorLeft.transform.position = gameObject.transform.position;
        DoorLeft.transform.parent = gameObject.transform;

        DoorLeft.transform.position += new Vector3(DoorLeft.transform.localPosition.x - distWall, DoorLeft.transform.localPosition.y, DoorLeft.transform.localPosition.z);

    }

    public void AlignWithPoint(Vector3 pointPos)
    {
        Bounds bounds = GetComponentInChildren<SpriteRenderer>().bounds;
        float distWall = bounds.size.x / 2;

        pointPos.x += distWall;

        transform.position = pointPos;
    }

    public float getHeight()
    {
        Bounds bounds = GetComponentInChildren<SpriteRenderer>().bounds;
        return bounds.size.y;
    }

    public float getWidth()
    {
        Bounds bounds = GetComponentInChildren<SpriteRenderer>().bounds;
        return bounds.size.x;
    }

}
