﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections.Generic;

public class ProcWorld : MonoBehaviour
{
    public static ProcWorld instance;
    private Vector4 bounds;

    public int nbLevel;
    public int levelSize;
    public int deltaSize;
    public float deltaAlign;

    public bool useSeed;

    public int debugSeed;

    public List<GameObject> small;
    public List<GameObject> middle;
    public List<GameObject> large;
    public GameObject elevator;

    public List<Vector3> roomsSpawn;
    // Use this for initialization

    void OnDrawGizmos()
    {
        Color oldCol = Gizmos.color;
        Gizmos.color = Color.green;
        foreach (Vector3 V in roomsSpawn)
        {
            Vector3 local = -transform.InverseTransformPoint(-V);
            Gizmos.DrawSphere(local + new Vector3(0, 0, -2), 0.2f);
        }
        Gizmos.color = oldCol;
    }

    void Awake()
    {
        instance = this;
        roomsSpawn = new List<Vector3>();
        //plus haut, plus bas, plus a gauche, plus a droite
        bounds = new Vector4(0,0,0,0);
    }

    void Start ()
    {

        if (useSeed)
            Random.seed = debugSeed;

    }

    public void GenerateWorld()
    {
        GameObject hotel = new GameObject();
        hotel.name = "Hotel";
        hotel.transform.SetParent(transform);

        float height = 0;
        //generate world
        for (int i = 0; i < nbLevel; i++)
        {
            bool elevatorIsPlaced = false;
            //gameObject pour garder une hierarchie propre
            GameObject level = new GameObject();
            level.name = "Level " + i;
            level.transform.SetParent(hotel.transform);

            //taille de couloir généré jusqu'a present
            int generatedSize = 0;
            //taille a atteindre
            int currentLevelSize = levelSize + Random.Range(-deltaSize, deltaSize);

            //precedente salle généré
            Room precRoom = null;

            while (generatedSize < currentLevelSize)
            {
                GameObject room = null;

                if (currentLevelSize - generatedSize >= (int)RoomSize.Large)
                {

                    if(currentLevelSize/2 - generatedSize <= (int)RoomSize.Large && elevatorIsPlaced == false)
                    {

                        if (currentLevelSize / 2 - generatedSize == 0)
                        {
                            room = elevator;
                            elevatorIsPlaced = true;
                        }

                        else if (currentLevelSize / 2 - generatedSize == 1)
                            room = GetRandomRoomInList(small);

                        else if (currentLevelSize / 2 - generatedSize == 2)
                            room = GetRandomRoomInList(middle);

                        else
                        {
                            room = getRandomRoom();
                        }
 
                        
                    }
                    else
                        //creation de la salle
                        room = getRandomRoom();
                }
                else
                {
                    if (currentLevelSize - generatedSize == 1)
                        room = GetRandomRoomInList(small);

                    if (currentLevelSize - generatedSize == 2)
                        room = GetRandomRoomInList(middle);

                    if (currentLevelSize - generatedSize == 3)
                        room = GetRandomRoomInList(large);
                }

                GameObject GO;
                if (generatedSize != currentLevelSize / 2)
                {
                    GO = Instantiate(room, new Vector3(0, 0, 0), gameObject.transform.rotation) as GameObject;
                    GO.GetComponent<RoomDecorationChooser>().ChooseDecoration();
                }
                else
                {
                    GO = Instantiate(elevator, new Vector3(0, 0, 0), gameObject.transform.rotation) as GameObject;
                    GO.GetComponent<ElevatorChooser>().backgroundIndex = i;
                }

                //nettoyage de la hierarchie
                GO.transform.SetParent(level.transform);

                Room actualRoom = GO.GetComponent<Room>();

                //positionnement de la salle
                if (height == 0)
                    height = actualRoom.getHeight();

                if (precRoom != null)
                    actualRoom.AlignWithPoint(precRoom.DoorRight.transform.position);
                else
                    actualRoom.AlignWithPoint(new Vector3(Random.Range(-deltaAlign, deltaAlign), height * i, 0));

                //enregistrement des spawnner
                foreach (Vector3 V in actualRoom.spawnPositions)
                {
                    roomsSpawn.Add(actualRoom.transform.TransformPoint(-V));
                }

                NetworkServer.Spawn(actualRoom.gameObject);

                //mise a jour des compteurs
                generatedSize += (int)actualRoom.roomSize;
                precRoom = actualRoom;
            }
        }


        CalculateBound();
    }

    //fonction placeholder
    GameObject getRandomRoom()
    {
        float rand = Random.Range(0, 100);

        if (rand < 33)
            return GetRandomRoomInList(small);
        if (rand > 33 && rand < 75)
            return GetRandomRoomInList(middle);
        if (rand > 75)
            return GetRandomRoomInList(large);

        return GetRandomRoomInList(small);
    }

    GameObject GetRandomRoomInList(List<GameObject> list)
    {
        return list[(int)Random.Range(0, list.Count)];
    }

    public Vector3 GetARoomSpawner()
    {
        int index = Random.Range(0, roomsSpawn.Count);
        return roomsSpawn[index];
    }

    public void RemoveSpawners()
    {
        roomsSpawn.Clear();
    }

    public Vector4 GetBounds()
    {
        return bounds;
    }

    void CalculateBound()
    {
        foreach (Vector3 pos in roomsSpawn)
        {
            if (pos.y > bounds.x)//plus haut
                bounds.x = pos.y;
            if (pos.y < bounds.y)//plus bas
                bounds.y = pos.y;
            if (pos.x > bounds.z)//plus gauche
                bounds.z = pos.x;
            if (pos.x < bounds.w)//plus droit
                bounds.w = pos.x;
        }
    }
}
