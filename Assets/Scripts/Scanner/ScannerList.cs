﻿using UnityEngine;
using System.Collections.Generic;

[CreateAssetMenu(fileName = "ScannerList", menuName = "Scanner/List", order = 1)]
public class ScannerList : ScriptableObject
{
    public string objectName = "ScannerList";
    public List<ScannerData> scanners;
}