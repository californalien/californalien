﻿using UnityEngine;
using System.Collections;

public class cameraUpdater : MonoBehaviour {

    Camera cam;
	// Use this for initialization
	void Start () {
        cam = GetComponent<Camera>();
	}
	
	// Update is called once per frame
	void Update () {
        cam.orthographicSize = Camera.main.orthographicSize;
	}
}
