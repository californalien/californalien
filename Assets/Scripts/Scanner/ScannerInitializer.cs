﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ScannerInitializer : MonoBehaviour {

    [Header("Gestion de l'init sur le reseau")]
    [Space]
    public Image handleDecoration;
    //public MeshRenderer colorFilter;
    public StencilHandler scannerFilter;


    //**************************
    //*******INIT SCANNER*******
    //**************************

    public void InitScanner(string dataName)
    {
        ScannerData scan = (ScannerData)Resources.Load("Data/Scanners/" + dataName);

        if (scan != null)
        {
            handleDecoration.sprite = scan.handleDecoration;
            //colorFilter.material = scan.colorfilterMat;
            scannerFilter.SetLayer(scan.layerFilter);

            //juste pour voir dans l'editeur
            scannerFilter.layer = scan.layerFilter;
        }
        else
        {
            Debug.Log("pas trouvé le scriptableObject");
        }
    }
}
