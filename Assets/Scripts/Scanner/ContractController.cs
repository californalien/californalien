﻿using UnityEngine;
using System.Collections;

public class ContractController : MonoBehaviour {

    [Header("Gestion du comportement")]
    [Space]
    public float movingAmount;

    public float autoCloseLimit;
    public float closingSpeed;

    private Vector3 initialPos;
    private Vector3 maxPos;
    private bool posMovement;

    private Vector3 screenPoint;
    private Vector3 offset;
    private bool drag = false;
    private int currentTouch;


    // Use this for initialization
    void Start()
    {

        initialPos = transform.localPosition;
        maxPos = new Vector3(transform.localPosition.x - movingAmount,
                              transform.localPosition.y,
                              transform.localPosition.z);
    }

    // Update is called once per frame
    void LateUpdate()
    {
        float xPos = 0;
        Vector3 newPos;
        newPos.x = transform.localPosition.x;

        xPos = Mathf.Clamp(transform.localPosition.x, maxPos.x, initialPos.x);

        newPos.x = xPos;

        newPos.y = transform.localPosition.y;

        newPos.z = transform.localPosition.z;
        transform.localPosition = newPos;

        if ((Mathf.Abs(transform.localPosition.x - initialPos.x) < autoCloseLimit) && !drag)
            AutoClose();
    }

    void AutoClose()
    {
        Vector3 newPos = transform.localPosition;
        newPos.x += closingSpeed;
        transform.localPosition = newPos;
    }

    public void OnDown()
    {
        if (MultiTouchController.Instance.GetCurrentTouchId(ref currentTouch))
            MultiTouchController.Instance.AddUsed(currentTouch);

        int index = 0;
        if (MultiTouchController.Instance.GetMyTouchIndex(currentTouch, ref index))
        {
            screenPoint = Camera.main.WorldToScreenPoint(gameObject.transform.position);
            offset = gameObject.transform.position - Camera.main.ScreenToWorldPoint(new Vector3(Input.touches[index].position.x, Input.touches[index].position.y, screenPoint.z));
        }
    }

    public void OnUp()
    {
        MultiTouchController.Instance.RemoveUsed(currentTouch);
        drag = false;
    }

    public void OnDrag()
    {
        int index = 0;
        if (MultiTouchController.Instance.GetMyTouchIndex(currentTouch, ref index))
        {
            drag = true;
            Vector3 cursorPoint = new Vector3(Input.touches[index].position.x, Input.touches[index].position.y, screenPoint.z);
            Vector3 cursorPosition = Camera.main.ScreenToWorldPoint(cursorPoint) + offset;
            transform.position = new Vector3(transform.position.x, cursorPosition.y, transform.position.z);
        }
    }
}
