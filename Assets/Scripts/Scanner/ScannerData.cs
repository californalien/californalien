﻿using UnityEngine;
using System.Collections;

[CreateAssetMenu(fileName = "Data", menuName = "Scanner/Data", order = 2)]
public class ScannerData : ScriptableObject
{
    public string scannerName = "";
    public Layer layerFilter;
    public Material colorfilterMat;
    public Sprite handleDecoration;
}

