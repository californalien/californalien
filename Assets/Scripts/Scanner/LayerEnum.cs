﻿using UnityEngine;
using System.Collections;


public enum Layer
{
    // Ne pas mettre de virgule après le dernier !
    Human = 0,
    Rock = 1,
    Furr = 2, 
    Smell = 3,
    Metal = 4,
    Tentacle = 5,
    Eye = 6,
    Reptile = 7,
    COUNT = 8,
    Spiked = 8
}

