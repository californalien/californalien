﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class StencilHandler : MonoBehaviour {

    Renderer rend;
    public Layer layer;
    public bool debugLayer;

	// Use this for initialization
	void Start () {
        
        if(debugLayer)
            SetLayer(layer);
    }

    public void SetLayer(Layer layer)
    {
        rend = GetComponent<Renderer>();
        rend.material.SetFloat("_Stencil", (float)layer);
    }

    public void SetLayer(string hash)
    {
        string[] type = hash.Split('_');
        layer = Layer.Human;

        rend = GetComponent<Renderer>();
        


        switch(type[1])
        {
            case "Rock":
                layer = Layer.Rock;
                break;
            case "Tentacle":
                layer = Layer.Tentacle;
                break;
            case "Eye":
                layer = Layer.Eye;
                break;
            case "Metal":
                layer = Layer.Metal;
                break;
            case "Smell":
                layer = Layer.Smell;
                break;
            case "Furr":
                layer = Layer.Furr;
                break;
            case "Reptile":
                layer = Layer.Reptile;
                break;
            case "Spiked":
                layer = Layer.Spiked;
                break;
        }

        rend.material.SetFloat("_Stencil", (float)layer);
    }

}
