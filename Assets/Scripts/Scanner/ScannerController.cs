﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ScannerController : MonoBehaviour {

    public enum Direction
    {
        Up,
        Down
    }

    [Header("Gestion de l'init sur le reseau")]
    [Space]
    public Image handleDecoration;
    public MeshRenderer colorFilter;
    public StencilHandler scannerFilter;


    [Header("Gestion du comportement")]
    [Space]
    public float movingAmount;
    public Direction direction;

    public float autoCloseLimit;
    public float closingSpeed;

    private Vector3 initialPos;
    private Vector3 maxPos;
    private bool posMovement;

    private Vector3 screenPoint;
    private Vector3 offset;
    private bool drag = false;
    private int currentTouch;
    

    // Use this for initialization
    void Start () {
        
        initialPos = transform.localPosition;
        maxPos = new Vector3( transform.localPosition.x,
                              transform.localPosition.y + (direction == Direction.Up ? movingAmount : -movingAmount),
                              transform.localPosition.z);
	}
	
	// Update is called once per frame
	void LateUpdate () {
        float yPos = 0;
        Vector3 newPos;
	    newPos.x = transform.localPosition.x;

        if (direction == Direction.Up)
        {
            yPos = Mathf.Clamp(transform.localPosition.y, initialPos.y, maxPos.y);
        }

        if (direction == Direction.Down)
        {
            yPos = Mathf.Clamp(transform.localPosition.y, maxPos.y, initialPos.y);
        }

        newPos.y = yPos;

        newPos.z = transform.localPosition.z;
        transform.localPosition = newPos;

        if((Mathf.Abs(transform.localPosition.y-initialPos.y) < autoCloseLimit) && !drag)
            AutoClose();
    }

    void AutoClose()
    {
        Vector3 newPos = transform.localPosition;
        newPos.y += (direction == Direction.Up ? -closingSpeed : closingSpeed);
        transform.localPosition = newPos;
    }

    public void OnDown()
    {
        if (MultiTouchController.Instance.GetCurrentTouchId(ref currentTouch))
            MultiTouchController.Instance.AddUsed(currentTouch);

        int index = 0;
        if(MultiTouchController.Instance.GetMyTouchIndex(currentTouch, ref index))
        {
            screenPoint = Camera.main.WorldToScreenPoint(gameObject.transform.position);
            offset = gameObject.transform.position - Camera.main.ScreenToWorldPoint(new Vector3(Input.touches[index].position.x, Input.touches[index].position.y, screenPoint.z));
        }
    }

    public void OnUp()
    {
        MultiTouchController.Instance.RemoveUsed(currentTouch);
        drag = false;
    }

    public void OnDrag()
    {
        int index = 0;
        if (MultiTouchController.Instance.GetMyTouchIndex(currentTouch, ref index))
        {
            drag = true;
            Vector3 cursorPoint = new Vector3(Input.touches[index].position.x, Input.touches[index].position.y, screenPoint.z);
            Vector3 cursorPosition = Camera.main.ScreenToWorldPoint(cursorPoint) + offset;
            transform.position = new Vector3(transform.position.x, cursorPosition.y, transform.position.z);
        }
    }

    //**************************
    //*******INIT SCANNER*******
    //**************************

    public void InitScanner(string dataName)
    {
        ScannerData scan = (ScannerData)Resources.Load("Data/Scanners/" + dataName);

        if (scan != null)
        {
            handleDecoration.sprite = scan.handleDecoration;
            colorFilter.material = scan.colorfilterMat;
            scannerFilter.SetLayer(scan.layerFilter);

            //juste pour voir dans l'editeur
            scannerFilter.layer = scan.layerFilter;
        }
        else
        {
            Debug.Log("pas trouvé le scriptableObject");
        }
}

}
