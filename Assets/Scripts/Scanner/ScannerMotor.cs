﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using FMOD.Studio;
using FMODUnity;

public class ScannerMotor : MonoBehaviour
{

    public enum Direction
    {
        Up,
        Down,
        Left,
        Right
    }

    [Header("Gestion du comportement")]
    [Space]
    public float movingAmount;
    public Direction direction;

    public float autoCloseLimit;
    public float closingSpeed;

    private Vector3 initialPos;
    private Vector3 maxPos;
    private bool posMovement;

    private Vector3 screenPoint;
    private Vector3 offset;
    private bool drag = false;
    private int currentTouch;

    EventInstance bruitage;
    ParameterInstance moving;


    // Use this for initialization
    void Awake()
    {
        Invoke("InitPosition", 0.5f);
        enabled = false;

    }

    void Start()
    {
        bruitage = RuntimeManager.CreateInstance("event:/scanner");
        bruitage.getParameter("lache", out moving);
    }

    void OnDestroy()
    {
        bruitage.stop(STOP_MODE.ALLOWFADEOUT);
        bruitage.release();
    }

    void InitPosition()
    {
        initialPos = transform.localPosition;

        if (direction == Direction.Up || direction == Direction.Down)
            maxPos = new Vector3(transform.localPosition.x,
                                  transform.localPosition.y + (direction == Direction.Up ? movingAmount : -movingAmount),
                                  transform.localPosition.z);

        if (direction == Direction.Left || direction == Direction.Right)
            maxPos = new Vector3(transform.localPosition.x + (direction == Direction.Right ? movingAmount : -movingAmount),
                                  transform.localPosition.y,
                                  transform.localPosition.z);

        enabled = true;
    }

    // Update is called once per frame
    void LateUpdate()
    {
        float yPos = transform.localPosition.y;
        float xPos = transform.localPosition.x;
        Vector3 newPos;

        if (direction == Direction.Right)
        {
            xPos = Mathf.Clamp(transform.localPosition.x, initialPos.x, maxPos.x);
        }
        else if (direction == Direction.Left)
        {
            xPos = Mathf.Clamp(transform.localPosition.x, maxPos.x, initialPos.x);
        }
        newPos.x = xPos ;

        if (direction == Direction.Up)
        {
            yPos = Mathf.Clamp(transform.localPosition.y, initialPos.y, maxPos.y);
        }
        else if (direction == Direction.Down)
        {
            yPos = Mathf.Clamp(transform.localPosition.y, maxPos.y, initialPos.y);
        }

        newPos.y = yPos;

        newPos.z = transform.localPosition.z;
        transform.localPosition = newPos;

        if (direction == Direction.Up || direction == Direction.Down)
            if ((Mathf.Abs(transform.localPosition.y - initialPos.y) < autoCloseLimit) && !drag)
            AutoClose();

        if (direction == Direction.Left || direction == Direction.Right)
            if ((Mathf.Abs(transform.localPosition.x - initialPos.x) < autoCloseLimit) && !drag)
            AutoClose();
    }

    void AutoClose()
    {
        Vector3 newPos = transform.localPosition;

        if (direction == Direction.Up || direction == Direction.Down)
            newPos.y += (direction == Direction.Up ? -closingSpeed : closingSpeed);
        

        if (direction == Direction.Right || direction == Direction.Left)
            newPos.x += (direction == Direction.Right ? -closingSpeed : closingSpeed);

        transform.localPosition = newPos;
    }

    public void OnDown()
    {
        if (!isActiveAndEnabled) return;

        bruitage.start();

        if (MultiTouchController.Instance.GetCurrentTouchId(ref currentTouch))
            MultiTouchController.Instance.AddUsed(currentTouch);

        int index = 0;
        if (MultiTouchController.Instance.GetMyTouchIndex(currentTouch, ref index))
        {
            screenPoint = Camera.main.WorldToScreenPoint(gameObject.transform.position);
            offset = gameObject.transform.position - Camera.main.ScreenToWorldPoint(new Vector3(Input.touches[index].position.x, Input.touches[index].position.y, screenPoint.z));
        }
    }

    public void OnUp()
    {
        if (!isActiveAndEnabled) return;

        moving.setValue(1f);

        MultiTouchController.Instance.RemoveUsed(currentTouch);
        drag = false;
    }

    public void OnDrag()
    {
        if (!isActiveAndEnabled) return;

        int index = 0;
        if (MultiTouchController.Instance.GetMyTouchIndex(currentTouch, ref index))
        {
            drag = true;
            Vector3 cursorPoint = new Vector3(Input.touches[index].position.x, Input.touches[index].position.y, screenPoint.z);
            Vector3 cursorPosition = Camera.main.ScreenToWorldPoint(cursorPoint) + offset;

            if (direction == Direction.Up || direction == Direction.Down)
                transform.position = new Vector3(transform.position.x, cursorPosition.y, transform.position.z);

            if (direction == Direction.Right || direction == Direction.Left)
                transform.position = new Vector3(cursorPosition.x, transform.position.y, transform.position.z);
        }
    }
}
