﻿using UnityEngine;
using System.Collections;

public class ScannersHandler : MonoBehaviour {

    [Header("Scanner dans l'ordre d'apparition")]
    [Space]
    public GameObject[] scanners;
    private int indexScanner =0;

    // Use this for initialization
    void Start () {
	
	}

    public void AddNewScanner(string scannerType)
    {
        if (indexScanner < scanners.Length)
        {
            scanners[indexScanner].gameObject.GetComponent<ScannerInitializer>().InitScanner(scannerType);
            scanners[indexScanner].gameObject.SetActive(true);
            indexScanner++;
        }
    }

    public void SetActivationAll(bool state)
    {
        foreach (GameObject go in scanners)
            go.SetActive(state);
    }

    public void SetActivationAllPreviousActivated(bool state)
    {
        for(int i = 0 ; i < scanners.Length;i++)
            scanners[i].SetActive(state);
    }

}
