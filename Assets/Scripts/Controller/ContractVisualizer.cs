﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using com.ootii.Messages;

public class ContractVisualizer : MonoBehaviour {

    [Header("Alien recherché par le joueur")]
    [Space]
    [HideInInspector]
    public AlienData alienWanted;
    public AlienDataBase database;
    public AlienStoryDataBase storyDatabase;

    [Header("Images des caracs")]
    [Space]
    public List<Sprite> Rock;
    public List<Sprite> Furr;
    public List<Sprite> Smell;
    public List<Sprite> Metal;
    public List<Sprite> Tentacle;
    public List<Sprite> Eye;
    public List<Sprite> Reptile;
    public List<Sprite> Spiked;

    [Header("Images des logo")]
    [Space]
    public Sprite RockLogo;
    public Sprite FurrLogo;
    public Sprite SmellLogo;
    public Sprite MetalLogo;
    public Sprite TentacleLogo;
    public Sprite EyeLogo;
    public Sprite ReptileLogo;
    public Sprite SpikedLogo;

    [Header("Image de l'alien")]
    [Space]
    public Image head;
    public Image torso;
    public Image arms;
    public Image legs;


    [Header("Logo du menu")]
    [Space]
    public Image headLogo;
    public Image torsoLogo;
    public Image armsLogo;
    public Image legsLogo;

    [Header("Texte")]
    [Space]

    public Text alienName;
    public Text alienStory;

    private HC_InGamePlayer localPlayer;
    public textAnimator textAtorName, textAtorStory;
    private bool hasNoContract = false;

    public GameObject contract,noContract, icons;
    public bool tuto;


    // Use this for initialization
    void Awake ()
    {
        MessageDispatcher.AddListener("ON_BEGIN_MAJINFO", MAJInfo);
        
    }

    void Start()
    {
        AlienStory story = storyDatabase.stories[Random.Range(0, storyDatabase.stories.Count)];
        textAtorName.toAnimate = story.Name;
        textAtorStory.toAnimate = story.Story;
    }

    void MAJInfo(IMessage mess)
    {
        if (tuto)
            return;

        localPlayer = GameController.instance.localPlayer;

        if(localPlayer.myContract == -1)
        {
            hasNoContract = true;
            SwapScreen();
            return;
        }
        else
        {
            if (hasNoContract)
            {
                SwapScreen();
                hasNoContract = false;
            }
                
        }

        

        alienWanted = database.culprits[localPlayer.myContract].alienCulprit;

        head.sprite = GetCaracIcon(alienWanted.Head,0);
        torso.sprite = GetCaracIcon(alienWanted.Body,1);
        arms.sprite = GetCaracIcon(alienWanted.Arms,2);
        legs.sprite = GetCaracIcon(alienWanted.Legs,3);

        headLogo.sprite = GetLogoIcon(alienWanted.Head);
        torsoLogo.sprite = GetLogoIcon(alienWanted.Body);
        armsLogo.sprite = GetLogoIcon(alienWanted.Arms);
        legsLogo.sprite = GetLogoIcon(alienWanted.Legs);

        if (alienWanted.HaveAStory)
        {
            /*textAtorName.toAnimate = alienWanted.Name;
            textAtorStory.toAnimate = alienWanted.Story;*/
            alienName.text = alienWanted.Name;
            alienStory.text = alienWanted.Story;
        }
        else
        {
            AlienStory story = storyDatabase.stories[Random.Range(0, storyDatabase.stories.Count)];
            /*textAtorName.toAnimate = story.Name;
            textAtorStory.toAnimate = story.Story;*/

            alienName.text = story.Name;
            alienStory.text = story.Story;
        }
    }

    Sprite GetLogoIcon(Layer layer)
    {
        Sprite toReturn = null;
        switch (layer)
        {
            case Layer.Rock:
                toReturn = RockLogo;
                break;
            case Layer.Furr:
                toReturn = FurrLogo;
                break;
            case Layer.Smell:
                toReturn = SmellLogo;
                break;
            case Layer.Metal:
                toReturn = MetalLogo;
                break;
            case Layer.Tentacle:
                toReturn = TentacleLogo;
                break;
            case Layer.Eye:
                toReturn = EyeLogo;
                break;
            case Layer.Reptile:
                toReturn = ReptileLogo;
                break;
            case Layer.Spiked:
                toReturn = SpikedLogo;
                break;
            default:
                break;
        }

        return toReturn;
    }

    Sprite GetCaracIcon(Layer layer,int index)
    {
        Sprite toReturn = null;
        switch (layer)
        {
            case Layer.Rock:
                toReturn = Rock[index];
                break;
            case Layer.Furr:
                toReturn = Furr[index];
                break;
            case Layer.Smell:
                toReturn = Smell[index];
                break;
            case Layer.Metal:
                toReturn = Metal[index];
                break;
            case Layer.Tentacle:
                toReturn = Tentacle[index];
                break;
            case Layer.Eye:
                toReturn = Eye[index];
                break;
            case Layer.Reptile:
                toReturn = Reptile[index];
                break;
            case Layer.Spiked:
                toReturn = Spiked[index];
                break;
            default:
                break;
        }

        return toReturn;
    }

    void SwapScreen()
    {
        if (contract.activeInHierarchy == true)
        {
            contract.SetActive(false);
            icons.SetActive(false);
            noContract.SetActive(true);
        }
        else
        {
            contract.SetActive(true);
            icons.SetActive(true);
            noContract.SetActive(false);
        }
        
        
    }

    void OnDestroy()
    {
        MessageDispatcher.RemoveListener("ON_NEW_CONTRACT", MAJInfo);
        MessageDispatcher.RemoveListener("ON_BEGIN_MAJINFO", MAJInfo);
    }
}
