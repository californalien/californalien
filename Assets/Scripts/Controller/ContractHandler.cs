﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections.Generic;
using com.ootii.Messages;

public class ContractHandler : NetworkBehaviour {

    public static ContractHandler instance;

    public int nbContracts = 4;

    private List<int> contractList;
    private List<int> givenContracts;

    public AlienDataBase database;
    private List<int> indexTake;
    private List<GameObject> culpritsGameObject;
    int counter = 0;
    //int alienCounter;

    void Awake()
    {
        instance = this;
        contractList = new List<int>();
        indexTake = new List<int>();
        culpritsGameObject = new List<GameObject>();
        givenContracts = new List<int>(); 
        GetNextContractSet();
    }

    public void AddCulpritsObject(GameObject obj)
    {
        culpritsGameObject.Add(obj);
    }

    public bool IsLastContract()
    {
        if (givenContracts.Count == 0 && contractList.Count == 0)
        {
            return true;
        }

        return false;
    }

    public List<int> GetCulpritsList()
    {
        return contractList;
    }

    public bool IsCulprit(GameObject obj)
    {
        if(culpritsGameObject.Contains(obj) && givenContracts.Contains(obj.GetComponent<AlienController>().culpritIndex) )
        return true;

        return false;
    }

    public void DestroyCulprit(GameObject obj)
    {
        culpritsGameObject.Remove(obj);
        givenContracts.Remove(obj.GetComponent<AlienController>().culpritIndex);
        Destroy(obj,0.2f);
        if (givenContracts.Count == 0 && contractList.Count == 0)
        {
            GameController.instance.RpcNotifyEndLevel();
            givenContracts.Clear();
            contractList.Clear();
            culpritsGameObject.Clear();
            counter = 0;
            //alienCounter = 0;
        }
    }

    void  NotifyMessage()
    {
        GameController.instance.RpcNotifyEndLevel();
    }

    public void GetNextContractSet()
    {
        givenContracts.Clear();
        contractList.Clear();
        culpritsGameObject.Clear();

        //alienCounter = 0;
        if (counter >= database.culprits.Count-15)
            indexTake.Clear();

        for (int i = 0; i < nbContracts; i++)
        {
            int index;
            do
            {
                index = Random.Range(0, database.culprits.Count-1);
            } while (indexTake.Contains(index) == true);

            indexTake.Add(index);
            counter++;

            contractList.Add(index);
        }
    }
    
    public int GetOneAlien()
    {
        if (contractList.Count > 0)
        {
            int contract = contractList[0];
            contractList.RemoveAt(0);
            givenContracts.Add(contract);
            return contract;
        }
        else
            return -1;
    }
}
