﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using com.ootii.Messages;

public class ImageLoader : MonoBehaviour {

    Image image;
    public float timeBetweenSlice;
    public int nbSlice = 200;
    public int minSlice = 1;
    public int maxSlice = 8;

    public float deltaBegin;

    public bool InOutAnim = true;

    private float sliceAmount;
    private int sliceCounter = 0;
    private float targetTime;

    private bool easeIn = true;

    void Awake()
    {
        MessageDispatcher.AddListener("ON_BEGIN_MAJINFO", OnAnimation);
        image = GetComponent<Image>();
    }

    void OnDestroy()
    {
        MessageDispatcher.RemoveListener("ON_BEGIN_MAJINFO", OnAnimation);
    }

    // Use this for initialization
    void Start()
    {
        image.fillOrigin = (int)Image.OriginVertical.Top;
        easeIn = true;
        sliceAmount = 1f / nbSlice;
        image.fillAmount = 0;
        targetTime = Time.time + timeBetweenSlice + deltaBegin;
    }

    // Update is called once per frame
    void Update()
    {
        if (easeIn)
            EaseIn();
        else if ( !easeIn && InOutAnim )
            EaseOut();
    }

    void EaseIn()
    {
        if (sliceCounter >= nbSlice || Time.time < targetTime) return;

        targetTime = Time.time + timeBetweenSlice;
        int slices = Random.Range(minSlice, maxSlice + 1);

        sliceCounter += slices;
        image.fillAmount += slices * sliceAmount;

        if(sliceCounter >= nbSlice)
        {
            if (InOutAnim)
            {
                targetTime = Time.time + timeBetweenSlice;
                image.fillOrigin = (int)Image.OriginVertical.Bottom;
            }
            else
            {
                this.enabled = false;
            }
        }
    }

    void EaseOut()
    {
        if (Time.time < targetTime) return;

        targetTime = Time.time + timeBetweenSlice;
        int slices = Random.Range(minSlice, maxSlice + 1);

        sliceCounter -= slices;
        image.fillAmount -= slices * sliceAmount;

        if (sliceCounter <= 0)
        {
            gameObject.SetActive(false);
        }
    }

    void OnEnable()
    {
        image.fillOrigin = (int)Image.OriginVertical.Top;
        easeIn = true;
        sliceCounter = 0;
        image.fillAmount = 0;
        targetTime = Time.time + timeBetweenSlice + deltaBegin;
    }

    void OnAnimation(IMessage mess)
    {
        easeIn = true;
        enabled = true;
    }
}