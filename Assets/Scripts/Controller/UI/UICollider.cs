﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class UICollider : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	void OnTriggerStay2D(Collider2D col)
    {
        ScrollRect myScroll = col.gameObject.GetComponentInParent<ScrollRect>();
        Debug.Log("STOP!");
        gameObject.GetComponentInParent<ScrollRect>().StopMovement();
        myScroll.StopMovement();
    }
}
