﻿using UnityEngine;
using System.Collections;
using com.ootii.Messages;

public class PhaseHandler : MonoBehaviour {

    public GameObject screenOnOff;
    public GameObject loading;

	// Use this for initialization
	void Start () {

        MessageDispatcher.AddListener("ON_LEVEL_LOAD",OnBeginLoad);

	}

    void OnDestroy()
    {
        MessageDispatcher.RemoveListener("ON_LEVEL_LOAD", OnBeginLoad);
    }

    // Update is called once per frame
    void Update () {
	
	}

    void OnBeginLoad(IMessage mess)
    {
        screenOnOff.SetActive(true);
        loading.SetActive(true);

       // MessageDispatcher.SendMessage("ON_END_LEVEL_LOAD", Random.Range(25f,55.5f));
    }
}
