﻿using UnityEngine;
using System.Collections;
using com.ootii.Messages;

public class CameraDrag : MonoBehaviour
{
    //comportement camera
    public float orthoZoomSpeed = 0.05f;
    public float minZoom = 1.0f;
    public float maxZoom = 20.0f;

    public float deltaBounds = 50;

    
    public float minX, maxX;
    
    public float minY, maxY;

    public float inertiaDuration = 1.0f;

    private float scrollVelocity = 0.0f;
    private float timeTouchPhaseEnded;
    private Vector2 scrollDirection = Vector2.zero;


    //gestion des doigts
    private int[] touchIds;
    private int[] touchIndexes;
    private int nbTouch = 0;

    Camera cam;

    Vector2?[] oldTouchPositions = {
        null,
        null
    };

    public static CameraDrag instance;

    //Vector2 oldTouchVector;
    //float oldTouchDistance;

    void Awake()
    {
        instance = this;
        MessageDispatcher.AddListener(OotiEvent.ON_END_LEVEL_LOAD.ToString(), GetBounds);
        cam = Camera.main;
    }

    void OnDestroy()
    {
        MessageDispatcher.RemoveListener(OotiEvent.ON_END_LEVEL_LOAD.ToString(), GetBounds);
    }

    // Use this for initialization
    void Start()
    {
        touchIndexes = new int[2];
        touchIds = new int[2] { 999, 999 };
    }


    void Update()
    {
        if (nbTouch == 0)
        {
            oldTouchPositions[0] = null;
            oldTouchPositions[1] = null;

            //if the camera is currently scrolling
            if (scrollVelocity != 0.0f)
            {
                //slow down over time
                float t = (Time.time - timeTouchPhaseEnded) / inertiaDuration;
                float frameVelocity = Mathf.Lerp(scrollVelocity, 0.0f, t);
                cam.transform.parent.position += -(Vector3)scrollDirection.normalized * (frameVelocity * 0.05f) * Time.deltaTime;

                if (t >= 1.0f)
                    scrollVelocity = 0.0f;
            }
        }
        else if (nbTouch == 1)
        {
            if (!MultiTouchController.Instance.GetMyTouchIndex(touchIds[0], ref touchIndexes[0]))
                return;

            Touch touch = Input.GetTouch(touchIndexes[0]);
            //quand on touche on arrete le scrolling inertiel
            if (touch.phase == TouchPhase.Began)
            {
                scrollVelocity = 0.0f;
            }
            //quand on bouge on bouge la camera
            else if (touch.phase == TouchPhase.Moved)
            {
                //beaucoup de choses a revoir ici
                if (oldTouchPositions[0] == null || oldTouchPositions[1] != null)
                {
                    oldTouchPositions[0] = Input.GetTouch(touchIndexes[0]).position;
                    oldTouchPositions[1] = null;
                }
                else {
                    Vector2 newTouchPosition = Input.GetTouch(touchIndexes[0]).position;
                    //deplacement en 1/1
                    cam.transform.parent.position += cam.transform.parent.TransformDirection((Vector3)((oldTouchPositions[0] - newTouchPosition) * cam.GetComponent<Camera>().orthographicSize / cam.GetComponent<Camera>().pixelHeight * 2f));

                    oldTouchPositions[0] = newTouchPosition;
                }

                scrollDirection = touch.deltaPosition.normalized;

                //bug qui met le touch.deltaTime a 0 causant un infinite causant un NaN
                if(touch.deltaTime != 0)
                    scrollVelocity = touch.deltaPosition.magnitude / touch.deltaTime;
                else
                    scrollVelocity = 0;
               
                if (scrollVelocity <= 100)
                    scrollVelocity = 0;
            }
            //quand on retire le doigt on met en place le compteur du scrolling inertiel
            else if (touch.phase == TouchPhase.Ended)
            {
                //deplacé dans OnUp a cause d'interaction avec le système de layerTouch
            }


        }
        else if (nbTouch == 2)
        {

            if (!MultiTouchController.Instance.GetMyTouchIndex(touchIds[0], ref touchIndexes[0]))
                return;

            if (!MultiTouchController.Instance.GetMyTouchIndex(touchIds[1], ref touchIndexes[1]))
                return;

            Vector2 cameraViewsize = new Vector2(cam.pixelWidth, cam.pixelHeight);

            Touch touchOne = Input.GetTouch(touchIndexes[0]);
            Touch touchTwo = Input.GetTouch(touchIndexes[1]);

            Vector2 touchOnePrevPos = touchOne.position - touchOne.deltaPosition;
            Vector2 touchTwoPrevPos = touchTwo.position - touchTwo.deltaPosition;

            float prevTouchDeltaMag = (touchOnePrevPos - touchTwoPrevPos).magnitude;
            float touchDeltaMag = (touchOne.position - touchTwo.position).magnitude;

            float deltaMagDiff = prevTouchDeltaMag - touchDeltaMag;

            cam.transform.parent.position += cam.transform.parent.TransformDirection((touchOnePrevPos + touchTwoPrevPos - cameraViewsize) * cam.orthographicSize / cameraViewsize.y);

            cam.orthographicSize += deltaMagDiff * orthoZoomSpeed;
            cam.orthographicSize = Mathf.Clamp(cam.orthographicSize, minZoom, maxZoom) - 0.001f;

            cam.transform.parent.position -= cam.transform.parent.TransformDirection((touchOne.position + touchTwo.position - cameraViewsize) * cam.orthographicSize / cameraViewsize.y);
        }
    }

    public void LateUpdate()
    {
        Vector3 pos = transform.parent.position;

        pos.x = Mathf.Clamp(pos.x, minX, maxX);
        pos.y = Mathf.Clamp(pos.y, minY, maxY);

        transform.parent.position = pos;
    }

    public void OnDown()
    {
        if (enabled == false)
            return;

        if (nbTouch < touchIds.Length)
            if (MultiTouchController.Instance.GetCurrentTouchId(ref touchIds[nbTouch]))
            {
                MultiTouchController.Instance.AddUsed(touchIds[nbTouch]);
                nbTouch++;
            }
    }

    public void OnUp()
    {
        if (enabled == false)
            return;

        oldTouchPositions[0] = null;
        oldTouchPositions[1] = null;
        int touchIndex = 0;
        for (int i = 0; i < touchIds.Length; i++)
        {
            if (MultiTouchController.Instance.GetMyTouchIndex(touchIds[i], ref touchIndex))
                if (Input.GetTouch(touchIndex).phase == TouchPhase.Ended)
                {
                    MultiTouchController.Instance.RemoveUsed(touchIds[i]);
                    nbTouch--;
                    touchIds[i] = 999;

                    if (nbTouch == 0)
                        timeTouchPhaseEnded = Time.time;
                }
        }
        TabSort();

    }

    public int GetDraggingTouchIndex()
    {
        int toReturn = 0;
        if (touchIds[0] != 999)
        {
            MultiTouchController.Instance.GetMyTouchIndex(touchIds[0], ref touchIndexes[0]);
            toReturn = touchIndexes[0];
        }

        if (touchIds[1] != 999)
        {
            MultiTouchController.Instance.GetMyTouchIndex(touchIds[1], ref touchIndexes[1]);
            toReturn = touchIndexes[1];
        }

        return toReturn;
    }


    public void TabSort()
    {
        bool needSort = true;

        while (needSort)
        {
            needSort = false;
            for (int i = 0; i < touchIds.Length - 1; i++)
            {
                if (touchIds[i] > touchIds[i + 1])
                {
                    int tmp = touchIds[i];
                    touchIds[i] = touchIds[i + 1];
                    touchIds[i + 1] = tmp;
                    needSort = true;
                }
            }
        }
    }

    

    void GetBounds(IMessage message)
    {
        if(GameController.instance != null)
        {
            GameController.instance.localPlayer.CmdGetWorldBounds();

            Invoke("GetBoundsInternal", 0.3f);
        }
 
    }

    void GetBoundsInternal()
    {
        Vector4 bounds = GameController.instance.localPlayer.Bounds;

        maxY = bounds.x + deltaBounds;
        minY = bounds.y - deltaBounds;
        maxX = bounds.z + deltaBounds;
        minX = bounds.w - deltaBounds;
    }
}


/*
using UnityEngine;
using System.Collections;

public class CameraDrag : MonoBehaviour
{

    public float mouseSensitivity = 1.0f;
    private Vector3 lastPosition ;

    private Vector3 _velocity;
    private bool _underInertia;
    private float _time = 0.0f;
    public float SmoothTime;
    Vector3 _curPosition;
    Vector3 _screenPoint;


    public float orthZoomStep = 10.0f;
    public int orthZoomMaxSize = 500;
    public int orthZoomMinSize = 300;
    public bool canMove = true;
    private bool orthographicView = true;

    void Update()
    {
        zoomCamera();

        if(canMove)
            MoveCamera();

        if (_underInertia && _time <= SmoothTime)
        {
            transform.position += _velocity;
            _velocity = Vector3.Lerp(_velocity, Vector3.zero, _time);
            _time += Time.smoothDeltaTime;
        }
        else
        {
            _underInertia = false;
            _time = 0.0f;
        }
    }

    public void MoveCamera()
    {
        if (Input.GetMouseButtonDown(0))
        {
            _screenPoint = Camera.main.WorldToScreenPoint(Input.mousePosition);
            _underInertia = false;
            lastPosition = Input.mousePosition;
        }

        if (Input.GetMouseButton(0))
        {
            Vector3 _prevPosition = _curPosition;
            Vector3 _curScreenPoint = new Vector3(Input.mousePosition.x, Input.mousePosition.y, _screenPoint.z);
            _curPosition = Camera.main.ScreenToWorldPoint(_curScreenPoint);

            var delta = Input.mousePosition - lastPosition;
            _velocity = _curPosition - _prevPosition;
            transform.Translate(delta.x * mouseSensitivity, delta.y * mouseSensitivity, 0);
            lastPosition = Input.mousePosition;
        }

        if (Input.GetMouseButtonUp(0))
        {
            _underInertia = true;
        }
    }

    void zoomCamera()
    {
        // zoom out
        if (Input.GetAxis("Mouse ScrollWheel") < 0)
        {
            if (orthographicView)
            {
                if (Camera.main.orthographicSize <= orthZoomMaxSize)
                    Camera.main.orthographicSize += orthZoomStep;
            }
            else
            {
                if (Camera.main.fieldOfView <= 150)
                    Camera.main.fieldOfView += 5;
            }
        }
        // zoom in
        if (Input.GetAxis("Mouse ScrollWheel") > 0)
        {
            if (orthographicView)
            {
                if (Camera.main.orthographicSize >= orthZoomMinSize)
                    Camera.main.orthographicSize -= orthZoomStep;
            }
            else
            {
                if (Camera.main.fieldOfView > 2)
                    Camera.main.fieldOfView -= 5;
            }
        }
    }
}
*/
