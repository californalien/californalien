﻿using UnityEngine;
using System.Collections.Generic;

public class MultiTouchController : MonoBehaviour
{
    public static MultiTouchController Instance;
    
    private List<int> usedFingerId;


	// Use this for initialization
	void Awake ()
    {
        usedFingerId = new List<int>();
        Instance = this;
	}
	
    public bool IsUsed(int value)
    {
        return usedFingerId.Contains(value);
    }

    public void AddUsed(int value)
    {
        usedFingerId.Add(value);
    }

    public void RemoveUsed(int value)
    {
        usedFingerId.Remove(value);
    }

    public bool GetCurrentTouchId(ref int value)
    {
        bool touchDetected = false;

        for(int i = 0; i< Input.touches.Length;i++)
        {
            if (Input.touches[i].phase == TouchPhase.Began)
            {
                if (!usedFingerId.Contains(Input.touches[i].fingerId))
                {
                    value = Input.touches[i].fingerId;
                    touchDetected = true;
                }
            }

        }
        return touchDetected;
    }

    public bool GetMyTouchIndex(int fingerId, ref int index)
    {
        bool isPresent = false;

        for (int i = 0; i < Input.touches.Length; i++)
        {
            if (Input.touches[i].fingerId == fingerId)
            {
                isPresent = true;
                index = i;
            }
        }

        return isPresent;
    }


    public bool VerifyTouch(int fingerId)
    {
        bool isPresent = false;

        for (int i = 0; i < Input.touches.Length; i++)
        {
            if (Input.touches[i].fingerId == fingerId)
            {
                isPresent = true;
            }
        }

        return isPresent;
    }
}
