﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections.Generic;
using com.ootii.Messages;
using FMOD.Studio;
using FMODUnity;

public class HC_InGamePlayer : NetworkBehaviour {

    [HideInInspector]
    [SyncVar(hook = "MAJCamBounds")]
    public Vector4 Bounds;

    [SyncVar]
    public Color playerColor;

    public int nbScanner =2;

    [SyncVar(hook = "OnContract")]
    public int myContract = -99999;

    [SyncVar(hook = "SetMusic")]
    public int myInstrument = -99999;

    private ScannersHandler scan;

    public SyncListString scanners;

    public bool IamLocal = false;

    EventInstance music;
    ParameterInstance musicZoom, musicInstrument;
    CameraDrag cameraControl;
    Camera cam;

    void Awake()
    {
        scanners = new SyncListString();
        Invoke("GetCamera",0.15f);
    }

    void GetCamera()
    {
        cameraControl = CameraDrag.instance;
        cam = cameraControl.GetComponent<Camera>();
    }

    void OnDestroy()
    {
        
        MessageDispatcher.RemoveListener("ON_ASK_NEW_CONTRACT", OnAskNewContract);
    }

    void OnDisable()
    {
        if (music != null)
        {
            music.stop(STOP_MODE.IMMEDIATE);
            music.release();
        }
    }


    [Command]
    void CmdInitScannerList()
    {
        for (int i = 0; i < nbScanner; i++)
        {
            string scanName = ScannerListHandler.Instance.GetNextRandomScanner();
            Debug.Log(scanName);
            scanners.Add(scanName);
        }
    }

    [Command]
    public void CmdGetAContract()
    {
        myContract = ContractHandler.instance.GetOneAlien();
    }

    void OnContract(int value)
    {
        myContract = value;
        if (IamLocal)
        {
            if (myContract == -1)
            {
                MessageDispatcher.SendMessage("ON_BEGIN_MAJINFO");
            }
            else
            {
                MessageDispatcher.SendMessage("ON_NEW_CONTRACT");
            }
        }
            
    }

    public override void OnStartLocalPlayer()
    {
        //base.OnStartLocalPlayer();
        MessageDispatcher.AddListener("ON_ASK_NEW_CONTRACT", OnAskNewContract);
        IamLocal = true;
        Invoke("FirstInit", 0.5f);
        if (isServer)
        {
            SetMusicNotCallBack(0);
        }
        else
        {

            CmdGetMusic();
        }
       
    }

    void FirstInit()
    {
        scan = GameObject.FindObjectOfType<ScannersHandler>();
        GameController.instance.localPlayer = this;
        CmdInitScannerList();
        Invoke("InitScanners", 0.5f);
        CmdGetAContract();
    }

    void InitScanners()
    {
        for (int i = 0; i < scanners.Count; i++)
            scan.AddNewScanner(scanners[i]);
    }

    void OnAskNewContract(IMessage message)
    {
        CmdGetAContract();
    }

    [Command]
    public void CmdGetMusic()
    {
        myInstrument = GameController.instance.GetMusic();
    }


    public void SetMusic(int nbMusic)
    {
        if (isServer) return;
        myInstrument = nbMusic;
        
        music = RuntimeManager.CreateInstance("event:/musique");

        switch (nbMusic)
        {
            case 1:
                music.getParameter("zoom", out musicZoom);
                music.getParameter("basson", out musicInstrument);
                break;
            case 0:
                music.getParameter("zoom", out musicZoom);
                music.getParameter("drumson", out musicInstrument);
                break;
            case 2:
                music.getParameter("zoom", out musicZoom);
                music.getParameter("1leftpar", out musicInstrument);
                break;
            case 3:
                music.getParameter("zoom", out musicZoom);
                music.getParameter("organon", out musicInstrument);
                break;
            case 4:
                music.getParameter("zoom", out musicZoom);
                music.getParameter("pianoon", out musicInstrument);
                break;
        }

        music.start();
        musicZoom.setValue(0f);
        musicInstrument.setValue(1f);
    }

    public void SetMusicNotCallBack(int nbMusic)
    {

        myInstrument = nbMusic;
        
        music = RuntimeManager.CreateInstance("event:/musique");

        switch (nbMusic)
        {
            case 1:
                music.getParameter("zoom", out musicZoom);
                music.getParameter("basson", out musicInstrument);
                break;
            case 0:
                music.getParameter("zoom", out musicZoom);
                music.getParameter("drumson", out musicInstrument);
                break;
            case 2:
                music.getParameter("zoom", out musicZoom);
                music.getParameter("1leftpar", out musicInstrument);
                break;
            case 3:
                music.getParameter("zoom", out musicZoom);
                music.getParameter("organon", out musicInstrument);
                break;
            case 4:
                music.getParameter("zoom", out musicZoom);
                music.getParameter("pianoon", out musicInstrument);
                break;
        }

        music.start();
        musicZoom.setValue(0f);
        musicInstrument.setValue(1f);
    }

    void Update()
    {
        if(cameraControl != null)
        {
            if (musicZoom != null)
            {  
                musicZoom.setValue(Mathf.Abs((cam.orthographicSize - cameraControl.maxZoom)) / (cameraControl.maxZoom - cameraControl.minZoom));
            }
        }
    }

    [Command]
    public void CmdTryToArrest(NetworkInstanceId objNetId)
    {
        GameObject obj = NetworkServer.FindLocalObject(objNetId);
        if(obj != null)
        {
            if(ContractHandler.instance.IsCulprit(obj))
            {
                obj.GetComponent<OnDestroyTourist>().RpcDestroyAlien();
                ContractHandler.instance.DestroyCulprit(obj);

                GameController.instance.RpcVerifyMyAlien(obj.GetComponent<AlienController>().culpritIndex);
            }
            else
            {
                RpcMakeShake();
                TimerController.instance.RpcTimerMalus(20f);
            }
        }
    }

    [ClientRpc]
    public void RpcMakeShake()
    {
        CamShakeSimple.instance.MakeShake();
    }

    [Command]
    public void CmdGetWorldBounds()
    {
        Bounds = ProcWorld.instance.GetBounds();
    }

    public void MAJCamBounds(Vector4 value)
    {
        Bounds = value;
    }
}
