﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class TimerAffichage : MonoBehaviour {

    Text timerText;

	// Use this for initialization
	void Start ()
    {
        timerText = GetComponent<Text>();
	}
	
	// Update is called once per frame
	void Update ()
    {
        if(TimerController.instance != null)
        {
            float timer;
            timer = TimerController.instance.timer;
            string minutes = Mathf.Floor(timer / 60).ToString("00");
            string seconds = Mathf.Floor(timer % 60).ToString("00");

            timerText.text = minutes + ":" + seconds;

        }
    }
}
