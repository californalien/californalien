﻿using UnityEngine;
using System.Collections;
using com.ootii.Messages;
using FMOD.Studio;

public class ButtonStart : MonoBehaviour {
    public ServerNotifier ator;

	public void OnClick()
    {
        ator.reinit();
        FMODUnity.RuntimeManager.PlayOneShot("event:/boutok",new Vector3(0,0,0));
        SLML_Manager.Instance.GetComponent<MenuMusic>().OnEndDiscoTime();
        SLML_Manager.Instance.GetComponent<MenuMusic>().OnLobbyWait();
    }

    public void OnRelease()
    {
        FMODUnity.RuntimeManager.PlayOneShot("event:/boutback", new Vector3(0, 0, 0));
    }
}
