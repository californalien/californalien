﻿Shader "Stencil/SetToX"
{
	Properties
	{
		_Stencil("Stencil Ref", Float) = 0
		_StencilComp("Stencil Comparison", Float) = 8
	}
	SubShader
	{
		Tags
		{
			"Queue" = "Transparent"
			"IgnoreProjector" = "True"
			"RenderType" = "Transparent"
			"PreviewType" = "Plane"
			"CanUseSpriteAtlas" = "True"
		}
		LOD 100

		ZWrite Off
		ColorMask 0

		Pass{
			Stencil{
			Ref [_Stencil]
			Comp always
			Pass replace
			}
		}
	}
}
