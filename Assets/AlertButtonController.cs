﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using com.ootii.Messages;

public class AlertButtonController : MonoBehaviour {

    private Image button;

    public Sprite buttonNormal, buttonClicked;
    public GameObject led; 
    bool isCliked = false;

    void Start()
    {
        button = GetComponent<Image>();
        led.SetActive(false);
        MessageDispatcher.AddListener(OotiEvent.UNACTIVATE_ALERT.ToString(), UnActivate);
    }

    void OnDestroy()
    {
        MessageDispatcher.RemoveListener(OotiEvent.UNACTIVATE_ALERT.ToString(), UnActivate);
    }

    public void OnClik()
    {
        if(isCliked == false)
        {
            FMODUnity.RuntimeManager.PlayOneShot("event:/captureon", transform.position);
            button.sprite = buttonClicked;
            //led.sprite = ledClicked;
            GameController.instance.isInAlert = true;
            isCliked = true;
            led.SetActive(true);
        }
        else
        {
            FMODUnity.RuntimeManager.PlayOneShot("event:/captureoff", transform.position);
            button.sprite = buttonNormal;
            //led.sprite = ledNormal;
            GameController.instance.isInAlert = false;
            isCliked = false;
            led.SetActive(false);
        }
    }


    void UnActivate(IMessage mess)
    {
        FMODUnity.RuntimeManager.PlayOneShot("event:/captureoff", transform.position);
        button.sprite = buttonNormal;
        //led.sprite = ledNormal;
        GameController.instance.isInAlert = false;
        isCliked = false;
        led.SetActive(false);
    }

}
