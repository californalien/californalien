﻿using UnityEngine;
using System.Collections;

public class ParallaxRelative : MonoBehaviour
{

    public Vector2 parallaxSpeed;
    GameObject target;
    Material mat;

	// Use this for initialization
	void Start () {
        //target = Camera.main.transform.parent.gameObject;
        mat = GetComponent<MeshRenderer>().material;
    }
	
	// Update is called once per frame
	void Update ()
    {
        Vector2 offset = mat.mainTextureOffset + parallaxSpeed * Time.deltaTime;
        
        mat.SetTextureOffset("_MainTex", offset);
	}
}
