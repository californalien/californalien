﻿using UnityEngine;
using System.Collections;

public class DestroyAfterTime : MonoBehaviour {

    float time = 3f;
    float targetTime;
	// Use this for initialization
	void Start () {
        targetTime = Time.time + time;   
	}
	
	// Update is called once per frame
	void Update () {
        if (Time.time > targetTime)
            Destroy(gameObject);
	}
}
