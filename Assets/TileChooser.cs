﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;
using System.Collections.Generic;

public class TileChooser : NetworkBehaviour {

    public List<Sprite> Collection;

    [SyncVar (hook= "MajSPrite")]
    private int index = -999;

	// Use this for initialization
	void Start ()
    {
	    
            index = Random.Range(0, Collection.Count);
        GetComponent<SpriteRenderer>().sprite = Collection[index];
    }

	
	void MajSPrite(int value)
    {
        index = value;
        GetComponent<SpriteRenderer>().sprite = Collection[index];
    }
}
