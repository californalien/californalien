﻿using UnityEngine;
using System.Collections;
using com.ootii.Messages;
using FMOD.Studio;
using FMODUnity;

public class VaultController : MonoBehaviour {

    private Animator ator;

    void Awake()
    {
        ator = GetComponent<Animator>();
        MessageDispatcher.AddListener("ON_LEVEL_LOAD", OnLevelLoad);
        MessageDispatcher.AddListener(OotiEvent.ON_GAME_END.ToString(), OnLevelLoad);
        MessageDispatcher.AddListener("ON_END_LEVEL_LOAD", OnEndLevelLoad);
    }

	// Use this for initialization
	void OnDestroy ()
    {
        MessageDispatcher.RemoveListener("ON_LEVEL_LOAD", OnLevelLoad);
        MessageDispatcher.RemoveListener("ON_END_LEVEL_LOAD", OnEndLevelLoad);
        MessageDispatcher.RemoveListener(OotiEvent.ON_GAME_END.ToString(), OnLevelLoad);
    }

    void OnLevelLoad(IMessage message)
    {
        ator.SetTrigger("Close");
    }

    void OnEndLevelLoad(IMessage message)
    {
        ator.SetTrigger("Open");
    }

    public void OnOpening()
    {
        MessageDispatcher.SendMessage("ON_LEVEL_BEGIN");
        
    }

    public void PlayCloseSound()
    {
        FMODUnity.RuntimeManager.PlayOneShot("event:/slam", transform.position);

    }

    public void PlayOpenSound()
    {
        FMODUnity.RuntimeManager.PlayOneShot("event:/ouvcont", transform.position);

    }

}
