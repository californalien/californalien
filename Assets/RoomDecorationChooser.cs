﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections.Generic;

public class RoomDecorationChooser : NetworkBehaviour {

    [SyncVar]
    int decorationForeIndex =-1;

    [SyncVar]
    int decorationBackIndex = -1;

    public List<Sprite> collectionForeground;
    public List<Sprite> collectionBackground;
    public SpriteRenderer foreground;
    public SpriteRenderer background;



    public void Start()
    {
        foreground.sprite = collectionForeground[decorationForeIndex];

        background.sprite = collectionBackground[decorationBackIndex];
    }

    public void ChooseDecoration()
    {
        if (collectionForeground.Count != 0)
            decorationForeIndex = (int)Random.Range(0, collectionForeground.Count);

        if (collectionBackground.Count != 0)
            decorationBackIndex = (int)Random.Range(0, collectionBackground.Count);
    }


}
