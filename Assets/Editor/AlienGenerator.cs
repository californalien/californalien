﻿using UnityEngine;
using UnityEditor;
using System.Collections.Generic;

public class AlienGenerator : MonoBehaviour
{
    [MenuItem("AlienGenerator/Make a new Alien")]
    static void MakeNewAlien()
    {
        //on ajoute le dossier du coupable
        string gui = AssetDatabase.CreateFolder("Assets/Resources/Data/Aliens/Culprits", "Alien1");
        string folder = AssetDatabase.GUIDToAssetPath(gui);

        //creation de scriptable object du coupable
        AlienData data = (AlienData)ScriptableObject.CreateInstance("AlienData");

        string[] folderSplited = folder.Split('/');

        AssetDatabase.CreateAsset(data, folder + "/Culprit" + folderSplited[folderSplited.Length-1] + ".asset");
        data.OnValidate();

        gui = AssetDatabase.CreateFolder(folder, "BadClones");
        folder = AssetDatabase.GUIDToAssetPath(gui);

        //creation de scriptable object du coupable
        data = (AlienData)ScriptableObject.CreateInstance("AlienData");
        AssetDatabase.CreateAsset(data, folder + "/BadClone1.asset");
        data.OnValidate();

        AssetDatabase.SaveAssets();
        AssetDatabase.Refresh();
    }

    [MenuItem("AlienGenerator/Generate random Alien")]
    static void GenerateRandomAlien()
    {
        for (int i = 0; i < 100; i++)
        {
            AlienData data = (AlienData)ScriptableObject.CreateInstance("AlienData");

            data.Head = (Layer)Random.Range(1, (int)(Layer.COUNT));
            data.Arms = (Layer)Random.Range(1, (int)(Layer.COUNT));
            data.Body = (Layer)Random.Range(1, (int)(Layer.COUNT));
            data.Legs = (Layer)Random.Range(1, (int)(Layer.COUNT));

            AssetDatabase.CreateAsset(data, "Assets/Resources/Data/Aliens/Randoms/RandomAlien" + i + ".asset");

            data.OnValidate();

        }

        AssetDatabase.SaveAssets();
        AssetDatabase.Refresh();

    }

    [MenuItem("AlienGenerator/Clear random Alien")]
    static void ClearRandomAlien()
    {
        for (int i = 0; i < 100; i++)
        {
            AssetDatabase.DeleteAsset("Assets/Resources/Data/Aliens/Randoms/RandomAlien" + i + ".asset");
        }

        AssetDatabase.SaveAssets();
        AssetDatabase.Refresh();
    }

    [MenuItem("AlienGenerator/Verify double")]
    static void VerifyDouble()
    {
        List<AlienData> data = GetAllRandom();

        string[] verifierAsset = AssetDatabase.FindAssets("t:AlienVerifier");

        string folder = AssetDatabase.GUIDToAssetPath(verifierAsset[0]);

        AlienVerifier verifier = AssetDatabase.LoadAssetAtPath<AlienVerifier>(folder);

        print("********VERIFICATION********");

        print("********VS_ALEATOIRE********");

        for (int i = 0; i < data.Count; i++)
        {
            for (int j = 0; j < verifier.culprits.Count; j++)
            {
                if (data[i].Hash == verifier.culprits[j].Hash && i != j)
                    print(data[i].name + " = " + verifier.culprits[j].name);
            }
        }

        print("*********VS_SUSPECT*********");

        for (int i = 0; i < verifier.suspects.Count; i++)
        {
            for (int j = 0; j < verifier.culprits.Count; j++)
            {
                if (verifier.suspects[i].Hash == verifier.culprits[j].Hash && i != j)
                    print(verifier.suspects[i].name + " = " + verifier.culprits[j].name);
            }
        }

        print("************FIN*************");

        Resources.UnloadUnusedAssets();
    }

    [MenuItem("AlienGenerator/Remove double")]
    static void RemoveDouble()
    {
        List<AlienData> data = GetAllRandom();

        string[] verifierAsset = AssetDatabase.FindAssets("t:AlienVerifier");

        string folder = AssetDatabase.GUIDToAssetPath(verifierAsset[0]);

        AlienVerifier verifier = AssetDatabase.LoadAssetAtPath<AlienVerifier>(folder);

        print("********VERIFICATION********");

        print("********VS_ALEATOIRE********");

        for (int i = 0; i < data.Count; i++)
        {
            for (int j = 0; j < verifier.culprits.Count; j++)
            {
                if (data[i].Hash == verifier.culprits[j].Hash && i != j)
                {
                    print(data[i].name + " = " + verifier.culprits[j].name + "  => REMOVING");
                    string toRemoveGui = AssetDatabase.FindAssets(data[i].name + " " + "t:AlienData")[0];
                    string toRemove = AssetDatabase.GUIDToAssetPath(toRemoveGui);
                    AssetDatabase.MoveAssetToTrash(toRemove);
                }
            }
        }

        print("*********VS_SUSPECT*********");

        for (int i = 0; i < verifier.suspects.Count; i++)
        {
            for (int j = 0; j < verifier.culprits.Count; j++)
            {
                if (verifier.suspects[i].Hash == verifier.culprits[j].Hash && i != j)
                {
                    print(verifier.suspects[i].name + " = " + verifier.culprits[j].name + "  => REMOVING");
                    string toRemoveGui = AssetDatabase.FindAssets(verifier.suspects[i].name + " " + "t:AlienData")[0];
                    string toRemove = AssetDatabase.GUIDToAssetPath(toRemoveGui);
                    AssetDatabase.MoveAssetToTrash(toRemove);
                }
            }
        }

        print("************FIN*************");

        AssetDatabase.SaveAssets();
        AssetDatabase.Refresh();

        Resources.UnloadUnusedAssets();
    }

    static List<AlienData> GetAllRandom()
    {
        List<AlienData> allData = new List<AlienData>();

        string randomRoot = "Assets/Resources/Data/Aliens/Randoms";

        string[] lookFor = new string[1];
        lookFor[0] = randomRoot;
        string[] data = AssetDatabase.FindAssets("t:AlienData", lookFor);

        for(int i = 0; i< data.Length;i++)
        {
            string folder = AssetDatabase.GUIDToAssetPath(data[i]);
            allData.Add(AssetDatabase.LoadAssetAtPath<AlienData>(folder));
        }

        return allData;
    }

}
