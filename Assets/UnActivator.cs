﻿using UnityEngine;
using System.Collections;

public class UnActivator : MonoBehaviour {

    public float timeBefore = 5f;
    private float targetTime;
	// Use this for initialization
	void Start () {
        targetTime = Time.time + timeBefore;
	}
	
	// Update is called once per frame
	void Update () {
        if (Time.time >= targetTime)
            gameObject.SetActive(false);
	}
}
