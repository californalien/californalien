﻿using UnityEngine;
using System.Collections;
using com.ootii.Messages;

public class ContractChanger : MonoBehaviour {


    private ScannerMotor motor;
    private float maxPos;
    public float animSpeed = 2f;
    bool open;
    bool isFirst = true;
    
    void Awake()
    {
        MessageDispatcher.AddListener("ON_NEW_CONTRACT", ActivateSelf);
        MessageDispatcher.AddListener("ON_END_LEVEL_LOAD", ActivateSelf);
        MessageDispatcher.AddListener("ON_LEVEL_LOAD", ActivateSelf2);
        MessageDispatcher.AddListener(OotiEvent.ON_LEVEL_LOAD.ToString(), Reload);
        motor = GetComponent<ScannerMotor>();
        maxPos = transform.localPosition.x + motor.movingAmount;
    }

    void OnDestroy()
    {
        MessageDispatcher.RemoveListener("ON_NEW_CONTRACT", ActivateSelf);
        MessageDispatcher.RemoveListener("ON_END_LEVEL_LOAD", ActivateSelf);
        MessageDispatcher.RemoveListener("ON_LEVEL_LOAD", ActivateSelf2);
        MessageDispatcher.RemoveListener(OotiEvent.ON_LEVEL_LOAD.ToString(), Reload);
    }

	// Use this for initialization
	void Start ()
    {
        motor.enabled = false;
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (open)
            Open();
        else
            Close();
	}

    void Open()
    {
        if (transform.localPosition.x < maxPos)
        {
            transform.localPosition = new Vector3(transform.localPosition.x + animSpeed * Time.deltaTime, transform.localPosition.y, transform.localPosition.z);
        }
        else
        {
            MessageDispatcher.SendMessage("ON_DRAWER_OPEN", 0.25f);
            motor.enabled = true;
            this.enabled = false;
        }
    }

    void Close()
    {
        if (transform.localPosition.x > 0)
        {
            transform.localPosition = new Vector3(transform.localPosition.x - animSpeed * Time.deltaTime, transform.localPosition.y, transform.localPosition.z);
        }
        else
        {
            motor.enabled = true;
            this.enabled = false;
        }
    }

    void Reload(IMessage mess)
    {
        isFirst = true;
    }

    void OnEnable()
    {
        motor.enabled = false;
    }

    void ActivateSelf(IMessage message)
    {
        Invoke("internalActivate", 0.8f);
    }
    void internalActivate()
    {
        if (isFirst)
        {
            isFirst = false;
            return;
        }
        open = true;
        enabled = true;
    }

    void ActivateSelf2(IMessage message)
    {
        Invoke("internalActivate2", 0.8f);
    }
    void internalActivate2()
    {

        isFirst = true;

        //open = false;
        //enabled = true;
    }
}
