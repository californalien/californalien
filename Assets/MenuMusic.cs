﻿using UnityEngine;
using System.Collections;
using FMOD.Studio;
using FMODUnity;
using UnityEngine.SceneManagement;

public class MenuMusic : MonoBehaviour {

    public static MenuMusic instance;
    EventInstance music;
    ParameterInstance musicStarship, musicMDR;

    // Use this for initialization
    void Start () {

        if (SceneManager.GetActiveScene().name != "scn_MenuLobby")
            return;

        music = RuntimeManager.CreateInstance("event:/menu");
        music.getParameter("verslobby", out musicStarship);
        music.getParameter("mdr", out musicMDR);
        music.start();
    }
	
	public void OnLobbyWait()
    {
        //musicStarship.setValue(1f);
    }

    public void OnEndLobbyWait()
    {
        musicStarship.setValue(0f);
    }

    public void OnDiscoTime()
    {
        musicMDR.setValue(1f);
        Invoke("OnEndDiscoTime", 30f);
    }

    public void OnEndDiscoTime()
    {
        musicStarship.setValue(0f);
        musicMDR.setValue(0f);
        CancelInvoke();
    }

    public void StopMusic()
    {
        music.stop(STOP_MODE.ALLOWFADEOUT);

        //music.release();
    }

    public void StartMusic()
    {
        if (music == null)
            return;

        if (SceneManager.GetActiveScene().name != "scn_MenuLobby")
            return;

        FMOD.Studio.PLAYBACK_STATE play_state;
        music.getPlaybackState(out play_state);
        if (play_state != FMOD.Studio.PLAYBACK_STATE.PLAYING)
        {
            music = RuntimeManager.CreateInstance("event:/menu");
            music.getParameter("verslobby", out musicStarship);
            music.getParameter("mdr", out musicMDR);
            musicStarship.setValue(0f);
            musicMDR.setValue(0f);
            music.start();
        }
    }

}
