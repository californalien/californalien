﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using com.ootii.Messages;

public class textAnimator : MonoBehaviour {

    private Text text;
    [TextArea(3,10)]
    public string toAnimate;
    public float animSpeed;
    private int counter = 0;
    //private float targetTime =0;

    void Awake()
    {
        MessageDispatcher.AddListener("ON_BEGIN_MAJINFO", OnNewContract);
        text = GetComponent<Text>();
    }

    void OnDestroy()
    {
        MessageDispatcher.RemoveListener("ON_BEGIN_MAJINFO", OnNewContract);
    }

	// Update is called once per frame
	void Update ()
    {
	    
	}

    void AddLetter()
    {
        if (counter >= toAnimate.Length)
        {
            CancelInvoke();
        }
        else
        {
            text.text = text.text + toAnimate[counter];
            //targetTime = Time.time + animSpeed;
            counter++;
        }
    }

    void OnEnable()
    {
        //counter = 0;
        //targetTime = Time.time + animSpeed;
        //text.text = "";
        //InvokeRepeating("AddLetter", animSpeed,animSpeed);
    }

    void OnNewContract(IMessage message)
    {
        //targetTime = 0;
        enabled = true;
    }
}
