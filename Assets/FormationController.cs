﻿using UnityEngine;
using System.Collections;
using com.ootii.Messages;

public class FormationController : MonoBehaviour {


    void Awake()
    {
        MessageDispatcher.AddListener("FORMATION_ALERT", ActiveSelf);
        gameObject.SetActive(false);
    }

    void OnDestroy()
    {
        MessageDispatcher.RemoveListener("FORMATION_ALERT", ActiveSelf);
    }

	public void Play()
    {
        FormationInfo.haveMadeFormation = true;
        gameObject.SetActive(false);
    }

    public void DontPlay()
    {
        gameObject.SetActive(false);
    }

    void ActiveSelf(IMessage mess)
    {
        gameObject.SetActive(true);
    }
}
