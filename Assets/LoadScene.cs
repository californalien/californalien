﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class LoadScene : MonoBehaviour {

    public string scene;
    public GameObject toDestroy;

	public void Click()
    {
        SLML_Manager.Instance.GetComponent<MenuMusic>().StopMusic();
        FormationInfo.haveMadeFormation = true;
        SceneManager.LoadScene(scene);
    }
}
