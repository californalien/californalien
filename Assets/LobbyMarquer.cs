﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;

public class LobbyMarquer : NetworkBehaviour {

    public GameObject toInstantiate;

	// Use this for initialization
	void Start ()
    {
        if(isLocalPlayer)
            Invoke("SetMarquer",0.8f);

    }

    void SetMarquer()
    {
        GameObject marquer = Instantiate(toInstantiate, transform.position, Quaternion.identity) as GameObject;

        marquer.transform.position = transform.position + new Vector3(0, -100, 0);
        marquer.transform.SetParent(gameObject.transform.parent.parent);
        marquer.transform.SetAsLastSibling();
    }
	
}
