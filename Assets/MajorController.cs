﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;
using com.ootii.Messages;

public class MajorController : MonoBehaviour {

    Animator ator;
    
    public OotiEvent OnEndAnimEvent;

    public bool destroyAfter;

    void NotifyEndLoading()
    {
        Invoke("UnActiveSelf", 0.1f);
        if (OnEndAnimEvent != OotiEvent.none)
        {
            MessageDispatcher.SendMessage(OnEndAnimEvent.ToString());
        }
    }

    void UnActiveSelf()
    {
        if (destroyAfter)
        {
            Destroy(transform.parent.gameObject);
        }
        else
        {
            gameObject.SetActive(false);
            gameObject.transform.parent.gameObject.SetActive(false);
        }
        
    }

    public void SetNextStep()
    {
        transform.parent.GetComponent<MajorRandom>().SetNextStep();
    }
}
