﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;
using Spriter2UnityDX;

public class HumanInitializer : NetworkBehaviour {

    [SyncVar]
    int humanIndex;

    public SpriteRenderer head;
    public SpriteRenderer eyes;
    public SpriteRenderer eyebrows;
    public SpriteRenderer mouth;
    public SpriteRenderer body;

    public SpriteRenderer leftUpperArm;
    public SpriteRenderer leftLowerArm;
    public SpriteRenderer leftHand;

    public SpriteRenderer rightUpperArm;
    public SpriteRenderer rightLowerArm;
    public SpriteRenderer rightHand;

    public SpriteRenderer leftUpperLeg;
    public SpriteRenderer leftLowerLeg;

    public SpriteRenderer rightUpperLeg;
    public SpriteRenderer rightLowerLeg;

    private CharacterInitializer initializer;

    // Use this for initialization
    void Start()
    {
        initializer = GetComponent<CharacterInitializer>();
        Invoke("PushCharaMap",0.8f);
        
    }

    void PushCharaMap()
    {
        AlienMap map = (AlienMap)Resources.Load("Data/Aliens/AlienMap/Humans/Human" + humanIndex); //;/+ (int)Random.Range(0, 33));
        //bouhhhh c'est trop caca ça...
        //mais pas le temps de faire beaucoup mieux..;

        if (map.head != null && head != null)
        {
            head.sprite = map.head;
            head.gameObject.GetComponent<StencilHandler>().SetLayer(initializer.head.GetComponent<StencilHandler>().layer);
        }

        if (map.eyes != null && eyes != null)
        {
            //for (int i = 0; i < map.eyes.Count; i++)
            //eyes.sprite = map.eyes[0];

            eyes.sprite = map.eyes;
            eyes.gameObject.GetComponent<StencilHandler>().SetLayer(initializer.eyes.GetComponent<StencilHandler>().layer);
        }

        if (map.eyebrows != null && eyebrows != null)
        {
            eyebrows.sprite = map.eyebrows;
            eyebrows.gameObject.GetComponent<StencilHandler>().SetLayer(initializer.eyebrows.GetComponent<StencilHandler>().layer);
        }

        if (map.mouth != null && mouth != null)
        {
            //for (int i = 0; i < map.mouth.Count; i++)
            //mouth.sprite = map.mouth[0];

            mouth.sprite = map.mouth;
            mouth.gameObject.GetComponent<StencilHandler>().SetLayer(initializer.mouth.GetComponent<StencilHandler>().layer);

        }

        if (map.body != null && body != null)
        {
            body.sprite = map.body;
            body.gameObject.GetComponent<StencilHandler>().SetLayer(initializer.body.GetComponent<StencilHandler>().layer);

        }

        if (map.leftUpperArm != null && leftUpperArm != null)
        {
            leftUpperArm.sprite = map.leftUpperArm;
            leftUpperArm.gameObject.GetComponent<StencilHandler>().SetLayer(initializer.leftUpperArm.GetComponent<StencilHandler>().layer);

        }

        if (map.leftLowerArm != null && leftLowerArm != null)
        {
            leftLowerArm.sprite = map.leftLowerArm;
            leftLowerArm.gameObject.GetComponent<StencilHandler>().SetLayer(initializer.leftLowerArm.GetComponent<StencilHandler>().layer);

        }

        if (map.leftHand != null && leftHand != null)
        {
            //for (int i = 0; i < map.leftHand.Count; i++)
            // leftHand.sprite = map.leftHand[0];

            leftHand.sprite = map.leftHand;
            leftHand.gameObject.GetComponent<StencilHandler>().SetLayer(initializer.leftHand.GetComponent<StencilHandler>().layer);

        }

        if (map.rightUpperArm != null && rightUpperArm != null)
        {
            rightUpperArm.sprite = map.rightUpperArm;
            rightUpperArm.gameObject.GetComponent<StencilHandler>().SetLayer(initializer.rightUpperArm.GetComponent<StencilHandler>().layer);

        }

        if (map.rightLowerArm != null && rightLowerArm != null)
        {
            rightLowerArm.sprite = map.rightLowerArm;
            rightLowerArm.gameObject.GetComponent<StencilHandler>().SetLayer(initializer.rightLowerArm.GetComponent<StencilHandler>().layer);

        }

        if (map.rightHand != null && rightHand != null)
        {
            /*for (int i = 0; i < map.rightHand.Count; i++)
                rightHand.sprite = map.rightHand[0];*/

            rightHand.sprite = map.rightHand;
            rightHand.gameObject.GetComponent<StencilHandler>().SetLayer(initializer.rightHand.GetComponent<StencilHandler>().layer);

        }

        if (map.leftUpperLeg != null && leftUpperLeg != null)
        {
            leftUpperLeg.sprite = map.leftUpperLeg;
            leftUpperLeg.gameObject.GetComponent<StencilHandler>().SetLayer(initializer.leftUpperLeg.GetComponent<StencilHandler>().layer);

        }

        if (map.leftLowerLeg != null && leftLowerLeg != null)
        {
            leftLowerLeg.sprite = map.leftLowerLeg;
            leftLowerLeg.gameObject.GetComponent<StencilHandler>().SetLayer(initializer.leftLowerLeg.GetComponent<StencilHandler>().layer);

        }

        if (map.rightUpperLeg != null && rightUpperLeg != null)
        {
            rightUpperLeg.sprite = map.rightUpperLeg;
            rightUpperLeg.gameObject.GetComponent<StencilHandler>().SetLayer(initializer.rightUpperLeg.GetComponent<StencilHandler>().layer);

        }

        if (map.rightLowerLeg != null && rightLowerLeg != null)
        {
            rightLowerLeg.sprite = map.rightLowerLeg;
            rightLowerLeg.gameObject.GetComponent<StencilHandler>().SetLayer(initializer.rightLowerLeg.GetComponent<StencilHandler>().layer);

        }
    }

    public void GenerateHuman(int value)
    {
        humanIndex = value;
    }

}
