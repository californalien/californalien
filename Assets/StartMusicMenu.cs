﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class StartMusicMenu : MonoBehaviour {

    void OnEnable()
    {
        if (SceneManager.GetActiveScene().name != "scn_MenuLobby")
            return;

        if(SLML_Manager.Instance != null)
            SLML_Manager.Instance.GetComponent<MenuMusic>().StartMusic();
    }
}
