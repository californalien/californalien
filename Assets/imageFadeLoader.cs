﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using com.ootii.Messages;

public class imageFadeLoader : MonoBehaviour {

    public float animTime;
    float stepAmount;
    private Image image;
    bool easeIn;
    public bool InOutAnim = true;

    void Awake()
    {
        MessageDispatcher.AddListener("ON_END_LEVEL_LOAD", OnEndAnimation);
        image = GetComponent<Image>();
    }

    void Destroy()
    {
        MessageDispatcher.RemoveListener("ON_END_LEVEL_LOAD", OnEndAnimation);
    }


    // Use this for initialization
    void Start () {
        stepAmount = 1f/animTime;
	}

    void Update()
    {
        if (easeIn)
            EaseIn();
        else if (!easeIn && InOutAnim)
            EaseOut();
    }

    void EaseIn()
    {
        image.color = new Color(image.color.r, image.color.g, image.color.b, image.color.a + stepAmount*Time.deltaTime);
    }

    void EaseOut()
    {
        image.color = new Color(image.color.r, image.color.g, image.color.b, image.color.a - stepAmount * Time.deltaTime);
    }

    void OnEnable()
    {
        easeIn = true;
        image.color = new Color(image.color.r, image.color.g, image.color.b, 0);
    }

    void OnEndAnimation(IMessage mess)
    {
        easeIn = false;
        //gameObject.SetActive(false);
    }
}
