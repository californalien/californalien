﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;

public class ProcDecoration : NetworkBehaviour {

    public GameObject right;
    public GameObject left;
    public GameObject bottom;
    public GameObject top;
    public GameObject topLeft;
    public GameObject topRight;

    void Start()
    {

    }

    public void GenerateWallLeft(Vector3 initialPos, int height, string name = "Wall")
    {
        GameObject wall = new GameObject();
        wall.name = name;
        wall.transform.SetParent(transform);

        wall.transform.position = initialPos;

        for (int i = 0; i <= height; i++)
        {
            GameObject tile;
            if (i != height )
               tile = Instantiate(left) as GameObject;
            else
               tile = Instantiate(topLeft) as GameObject;

            tile.transform.SetParent(wall.transform);
            tile.transform.localPosition = new Vector3(0, 0, -4);

            //recuperation des boundaries du sprite renderer
            Bounds tileBounds = tile.GetComponent<SpriteRenderer>().bounds;

            //positionenment de la tile en hauteur m
            tile.transform.localPosition = new Vector3(tile.transform.localPosition.x - tileBounds.extents.x,
                                                       tile.transform.localPosition.y + tileBounds.size.y * i,
                                                       tile.transform.localPosition.z);

            NetworkServer.Spawn(tile);
        }

    }

    public void GenerateWallRight(Vector3 initialPos, int height, int width, string name = "Wall")
    {
        GameObject wall = new GameObject();
        wall.name = name;
        wall.transform.SetParent(transform);

        wall.transform.position = initialPos;

        for (int i = 0; i <= height; i++)
        {
            GameObject tile;
            if (i != height)
                tile = Instantiate(right) as GameObject;
            else
                tile = Instantiate(topRight) as GameObject;

            float posx = tile.GetComponent<SpriteRenderer>().bounds.size.x * width;

            tile.transform.SetParent(wall.transform);
            tile.transform.localPosition = new Vector3(0, 0, -4);

            //recuperation des boundaries du sprite renderer
            Bounds tileBounds = tile.GetComponent<SpriteRenderer>().bounds;

            //positionenment de la tile en hauteur m
            tile.transform.localPosition = new Vector3(tile.transform.localPosition.x + posx + tileBounds.extents.x,
                                                       tile.transform.localPosition.y + tileBounds.size.y * i,
                                                       tile.transform.localPosition.z);

            NetworkServer.Spawn(tile);
        }

    }

    public void GenerateBottom(Vector3 initialPos, int width, string name = "Wall")
    {
        GameObject wall = new GameObject();
        wall.name = name;
        wall.transform.SetParent(transform);

        wall.transform.position = initialPos;

        for (int i = 0; i <= width; i++)
        {
            GameObject tile;

            tile = Instantiate(bottom) as GameObject;


            tile.transform.SetParent(wall.transform);
            tile.transform.localPosition = new Vector3(0, 0, -4);

            //recuperation des boundaries du sprite renderer
            Bounds tileBounds = tile.GetComponent<SpriteRenderer>().bounds;

            //positionenment de la tile en hauteur m
            tile.transform.localPosition = new Vector3(tile.transform.localPosition.x + tileBounds.size.x * i,
                                                       tile.transform.localPosition.y,
                                                       tile.transform.localPosition.z);

            NetworkServer.Spawn(tile);
        }

    }

    public void GenerateTop(Vector3 initialPos, int nbLevel,int width, string name = "Wall")
    {
        GameObject wall = new GameObject();
        wall.name = name;
        wall.transform.SetParent(transform);

        wall.transform.position = initialPos;

        for (int i = 0; i < width; i++)
        {
            GameObject tile;

            tile = Instantiate(top) as GameObject;


            tile.transform.SetParent(wall.transform);
            tile.transform.localPosition = new Vector3(0, 0, -4);

            //recuperation des boundaries du sprite renderer
            Bounds tileBounds = tile.GetComponent<SpriteRenderer>().bounds;

            //positionenment de la tile en hauteur m
            tile.transform.localPosition = new Vector3(tile.transform.localPosition.x + tileBounds.size.x  * i + tileBounds.extents.x,
                                                       tile.transform.localPosition.y + tileBounds.size.y * nbLevel,
                                                       tile.transform.localPosition.z);

            NetworkServer.Spawn(tile);
        }

    }

}
