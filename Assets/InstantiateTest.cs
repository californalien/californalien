﻿using UnityEngine;
using System.Collections;

public class InstantiateTest : MonoBehaviour {
    public GameObject toInstantiate;
	// Use this for initialization
	void Start () {
        Instantiate(toInstantiate);
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
