﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using com.ootii.Messages;

public class LobbyButtonViteTaf : MonoBehaviour {


    bool isActive = false;
    public Sprite actif;
    public Sprite nonActif;
    Image img;

    void Start()
    {
        img = GetComponent<Image>();
    }

	public void Click()
    {
        if(isActive == false)
        {
            img.sprite = actif;
            MessageDispatcher.SendMessage("ON_READY");
            isActive = true;
            FMODUnity.RuntimeManager.PlayOneShot("event:/boutok", transform.position);
        }
        else if (isActive == true)
        {
            img.sprite = nonActif;
            MessageDispatcher.SendMessage("ON_STOP_READY");
            isActive = false;
            FMODUnity.RuntimeManager.PlayOneShot("event:/boutback", transform.position);
        }

    }
}
