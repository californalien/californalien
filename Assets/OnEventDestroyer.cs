﻿using UnityEngine;
using System.Collections;
using com.ootii.Messages;

public class OnEventDestroyer : MonoBehaviour {

    public OotiEvent evt;

	// Use this for initialization
	void Start ()
    {
        MessageDispatcher.AddListener(evt.ToString(), DestroyMe);
	}
	
	void DestroyMe(IMessage message)
    {
        Destroy(gameObject);
    }
}
