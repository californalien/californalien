﻿using UnityEngine;
using System.Collections;
using com.ootii.Messages;

public class StampController : MonoBehaviour {

    Animator ator;

	// Use this for initialization
	void Awake () {
        MessageDispatcher.AddListener("ON_DRAWER_OPEN", BeginStamped);
        MessageDispatcher.AddListener("ON_LEVEL_LOAD", OnLoadLevel);
        ator = GetComponent<Animator>();
	}

    void OnDestroy()
    {
        MessageDispatcher.RemoveListener("ON_LEVEL_LOAD", OnLoadLevel);
        MessageDispatcher.RemoveListener("ON_DRAWER_OPEN", BeginStamped);
    }

    void BeginStamped(IMessage message)
    {
        ator.SetTrigger("Stamped");
        Invoke("setBool", 0.2f);
    }

    public void GetNewContract()
    {
        MessageDispatcher.SendMessage("ON_ASK_NEW_CONTRACT");
    }

    public void ActivateScreen()
    {
        MessageDispatcher.SendMessage("ON_BEGIN_MAJINFO");
    }

    void setBool()
    {
        ator.SetBool("First", false);
    }

    void OnLoadLevel(IMessage message)
    {
        ator.SetBool("First", true);
    }

    public void PlayStampSound()
    {
        FMODUnity.RuntimeManager.PlayOneShot("event:/chargecontrat", transform.position);
    }
}
