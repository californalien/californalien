﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;
using com.ootii.Messages;
using System.Collections.Generic;

public class MajorRandom : MonoBehaviour {

    public List<Animator> majors;
    public OotiEvent ActivateEvent;
    public bool autoIn;
    int index;


    public void Awake()
    {
        MessageDispatcher.AddListener(ActivateEvent.ToString(), ActiveSelf);
    }

    public void Start()
    {

        if (autoIn)
        {
            index = Random.Range(0, majors.Count);
            SetNextStep();
        }
        else
        {
            gameObject.SetActive(false);
        }


    }

    public void SetNextStep()
    {
        majors[index].SetTrigger("NextStep");
    }

    void ActiveSelf(IMessage message)
    {
        index = Random.Range(0, majors.Count);
        gameObject.SetActive(true);
        majors[index].gameObject.SetActive(true);
        Invoke("SetNextStep", 0.1f);
    }

    void OnDestroy()
    {
        MessageDispatcher.RemoveListener(ActivateEvent.ToString(), ActiveSelf);
    }
}
