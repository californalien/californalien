﻿using UnityEngine.Networking;
using UnityEngine;
using System.Collections.Generic;

public class ElevatorChooser : NetworkBehaviour
{

    public List<Sprite> background;
    public Sprite foreground;
    public SpriteRenderer rendererForeground;
    public SpriteRenderer rendererBackground;

    [SyncVar]
    public int backgroundIndex;

    public void Start()
    {
        rendererBackground.sprite = background[backgroundIndex];
        rendererForeground.sprite = foreground;
    }
	
}
