﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections.Generic;
using UnityEngine.UI;


public class DialogSequence : MonoBehaviour {

    public List<Text> sentences;
    public float timeBefore;
    public float timeBetween;
    private float targetTime;
    private int index = 0;
    private MajorController major;
    public Image portrait;

    // Use this for initialization
    void Awake()
    {
        targetTime = Time.time + timeBefore;
        major = GetComponentInParent<MajorController>();
    }

	// Update is called once per frame
	void Update ()
    {
	    if(Time.time >= targetTime && index < sentences.Count)
        {
            if (index == 0)
            {
                sentences[0].gameObject.SetActive(true);
                portrait.sprite = sentences[0].GetComponent<MajorPortrait>().portrait;
                index++;
            }
            else
            {
                sentences[index - 1].gameObject.SetActive(false);
                sentences[index].gameObject.SetActive(true);

                if(sentences[index].GetComponent<MajorPortrait>().portrait != null)
                    portrait.sprite = sentences[index].GetComponent<MajorPortrait>().portrait;

                index++;
            }

            if (index >= sentences.Count)
            {
                Invoke("SetTrigger",0.8f);
            }


            timeBetween = sentences[index-1].GetComponent<SentenceTimer>().timer;
            targetTime = Time.time + timeBetween;
        }
	}


    void OnEnable()
    {
        index = 0;
        targetTime = Time.time + timeBefore;

        foreach (Text txt in sentences)
            txt.gameObject.SetActive(false);
    }


    void SetTrigger()
    {
        major.SetNextStep();
    }
}
