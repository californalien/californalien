﻿using UnityEngine;
using System.Collections;
using com.ootii.Messages;

public class TutoContractChanger : MonoBehaviour
{

    private ScannerMotor motor;
    private float maxPos;
    private Vector3 initialPos;
    public float animSpeed = 2f;
    bool open;
    bool isFirst = true;
    bool haveOpen = false;
    public GameObject iconActivate;

    void Awake()
    {
        MessageDispatcher.AddListener(OotiEvent.OPEN_DRAWER.ToString(), ActivateSelf);
        MessageDispatcher.AddListener(OotiEvent.APPEAR_DRAWER_ICON.ToString(), ActivateIcon);
        motor = GetComponent<ScannerMotor>();
        maxPos = transform.localPosition.x + motor.movingAmount;
        initialPos = transform.localPosition;
    }

    void OnDestroy()
    {
        MessageDispatcher.RemoveListener(OotiEvent.OPEN_DRAWER.ToString(), ActivateSelf);
        MessageDispatcher.RemoveListener(OotiEvent.APPEAR_DRAWER_ICON.ToString(), ActivateIcon);
    }

    void ActivateIcon(IMessage mess)
    {
        iconActivate.SetActive(true);
    }

    // Use this for initialization
    void Start()
    {
        motor.enabled = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (haveOpen)
        {
            if (Vector3.Distance(transform.localPosition, initialPos) < 0.1)
            {
                MessageDispatcher.SendMessage(OotiEvent.DRAWER_CLOSED.ToString(), 0.25f);
                haveOpen = false;
                iconActivate.SetActive(false);
                this.enabled = false;
            }
        }


        if (open && haveOpen == false)
            Open();

    }

    void Open()
    {
        if (transform.localPosition.x < maxPos)
        {
            transform.localPosition = new Vector3(transform.localPosition.x + animSpeed * Time.deltaTime, transform.localPosition.y, transform.localPosition.z);
        }
        else
        {
            MessageDispatcher.SendMessage("ON_DRAWER_OPEN", 0.25f);
            motor.enabled = true;
            haveOpen = true;
            //this.enabled = false;
        }
    }

    void Close()
    {
        if (transform.localPosition.x > 0)
        {
            transform.localPosition = new Vector3(transform.localPosition.x - animSpeed * Time.deltaTime, transform.localPosition.y, transform.localPosition.z);
        }
        else
        {
            motor.enabled = true;
            this.enabled = false;
        }
    }

    void Reload(IMessage mess)
    {
        isFirst = true;
    }

    void OnEnable()
    {
        motor.enabled = false;
    }

    void ActivateSelf(IMessage message)
    {
        Invoke("internalActivate", 0.8f);
    }
    void internalActivate()
    {
        open = true;
        this.enabled = true;
        motor.enabled = false;
        
    }

    void ActivateSelf2(IMessage message)
    {
        Invoke("internalActivate2", 0.8f);
    }
    void internalActivate2()
    {

        isFirst = true;

        //open = false;
        //enabled = true;
    }
}

