﻿using UnityEngine;
using System.Collections;
using com.ootii.Messages;

public class EventActivator : MonoBehaviour {

    public OotiEvent activateEvent;

	// Use this for initialization
	void Awake ()
    {
        MessageDispatcher.AddListener(activateEvent.ToString(), activate);
        gameObject.SetActive(false);
    }

    void OnDestroy()
    {
        MessageDispatcher.RemoveListener(activateEvent.ToString(), activate);
    }
	
    void activate(IMessage mess)
    {
        gameObject.SetActive(true);
    }

}
