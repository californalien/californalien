﻿using UnityEngine;
using System.Collections;
using com.ootii.Messages;

public class TutoMarquer : MonoBehaviour {

    public OotiEvent eventTosend;
    public GameObject marqueur;
    bool marqueurActif = false;
    GameObject marqueurGo;
    public GameObject icone;
    public bool canBeCaptured;
    public GameObject toInstantiate;
    public OotiEvent destroyEvent;
    public float timerAfterDestroy = 0.8f;


    void Awake()
    {
        MessageDispatcher.AddListener(OotiEvent.ON_SCANNER_OPEN.ToString(), Activate);
        enabled = false;
    }

    void Activate(IMessage mess)
    {
        enabled = true;
    }

    void OnDestroy()
    {
        MessageDispatcher.RemoveListener(OotiEvent.ON_SCANNER_OPEN.ToString(), Activate);
    }

    public void OnMouseUpAsButton()
    {
        if(TutoAlertButton.instance.isInAlert == false)
        SetMarqueur();
        else
        {
            if(canBeCaptured)
            {
                if(destroyEvent != OotiEvent.none)
                    MessageDispatcher.SendMessage(destroyEvent.ToString(), timerAfterDestroy);

                FMODUnity.RuntimeManager.PlayOneShot("event:/jinvic", transform.position);
                Instantiate(toInstantiate, transform.position, transform.rotation);
                Destroy(gameObject);
            }
            else
            {
                Camera.main.GetComponent<CamShakeSimple>().MakeShake();
            }
            
        }
    }

    public void SetMarqueur()
    {
        if (enabled == false)
            return;

        if (!marqueurActif)
        {
            if (icone != null)
            {
                MessageDispatcher.SendMessage(eventTosend.ToString(),0.8f);
                Destroy(icone);
            }

            marqueurGo = Instantiate(marqueur, transform.position, transform.rotation) as GameObject;
            marqueurGo.transform.SetParent(transform);
            marqueurActif = true;
            Debug.Log(Camera.main.transform.parent.position);
            FMODUnity.RuntimeManager.PlayOneShot("event:/tag", transform.position);

        }
        else
        {
            Destroy(marqueurGo);
            Debug.Log(Camera.main.transform.parent.position);
            FMODUnity.RuntimeManager.PlayOneShot("event:/untag", transform.position);
            marqueurActif = false;
        }
    }
}
