﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using com.ootii.Messages;

public class TutoLoad : MonoBehaviour {

    public OotiEvent eventToLoad;
    public string scene;

	// Use this for initialization
	void Start () {
        MessageDispatcher.AddListener(eventToLoad.ToString(), LoadScene);
	}
	
	void LoadScene(IMessage mess)
    {
        SceneManager.LoadScene(scene);
    }
}
