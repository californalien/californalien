﻿using UnityEngine;
using System.Collections;
using com.ootii.Messages;

public class TutoSendEventIn : MonoBehaviour {

    public OotiEvent eventToSend;
    public float time;

	// Use this for initialization
	void Start()
    {
        //MessageDispatcher.AddListener(eventToSend.ToString(), SendEvent);
        if (eventToSend != OotiEvent.none)
            MessageDispatcher.SendMessage(eventToSend.ToString(), time);
    }


}
