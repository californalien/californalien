﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using com.ootii.Messages;

public class TutoAlertButton : MonoBehaviour {

    public static TutoAlertButton instance;
    private Image button;
    public GameObject toInactivate;

    public Sprite buttonNormal, buttonClicked;
    public GameObject led;
    bool isCliked = false;
    public bool isInAlert = false;
    public OotiEvent toSend;
    public OotiEvent activateEvent;
    public float sendIn = 0.8f;

    void Awake()
    {
        instance = this;
        MessageDispatcher.AddListener(activateEvent.ToString(), Activate);
        enabled = false;
    }

    void OnDestroy()
    {
        MessageDispatcher.RemoveListener(activateEvent.ToString(), Activate);
    }

    void Activate(IMessage mess)
    {
        enabled = true;
    }

    void Start()
    {
        button = GetComponent<Image>();
        led.SetActive(false);
    }

    public void OnClik()
    {
        if (enabled == false)
            return;
        if (isCliked == false)
        {
            if(toInactivate != null)
            {
                Destroy(toInactivate);
                MessageDispatcher.SendMessage(toSend.ToString(), sendIn);
            }
            button.sprite = buttonClicked;
            //led.sprite = ledClicked;
            isInAlert = true;
            isCliked = true;
            led.SetActive(true);
            FMODUnity.RuntimeManager.PlayOneShot("event:/captureon", transform.position);

        }
        else
        {
            FMODUnity.RuntimeManager.PlayOneShot("event:/captureoff", transform.position);
            button.sprite = buttonNormal;
            //led.sprite = ledNormal;
            isInAlert = false;
            isCliked = false;
            led.SetActive(false);
        }
    }
}
