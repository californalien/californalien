﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using com.ootii.Messages;

public class TutoTimerController : MonoBehaviour {

    Text timerText;
    public OotiEvent eventActivate;
    public OotiEvent toSend;
    public OotiEvent toSend2;
    public float timer;

    void Awake()
    {
        MessageDispatcher.AddListener(eventActivate.ToString(), Activate);
        enabled = false;
    }

    void Activate(IMessage mess)
    {
        enabled = true;
    }

    void OnDestroy()
    {
        MessageDispatcher.RemoveListener(eventActivate.ToString(), Activate);
    }

    // Use this for initialization
    void Start()
    {
        timerText = GetComponent<Text>();
    }

    // Update is called once per frame
    void Update()
    {
        if (timer > 0)
        {
            string minutes = Mathf.Floor(timer / 60).ToString("00");
            string seconds = Mathf.Floor(timer % 60).ToString("00");

            timerText.text = minutes + ":" + seconds;
            timer -= Time.deltaTime;
        }
        else
        {
            timer = 0;
            string minutes = Mathf.Floor(timer / 60).ToString("00");
            string seconds = Mathf.Floor(timer % 60).ToString("00");

            timerText.text = minutes + ":" + seconds;

            MessageDispatcher.SendMessage(toSend.ToString());
            MessageDispatcher.SendMessage(toSend2.ToString());
            enabled = false;
        }
        
    }
}
