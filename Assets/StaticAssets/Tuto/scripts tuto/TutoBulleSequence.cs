﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using com.ootii.Messages;

public class TutoBulleSequence : MonoBehaviour
{

    public List<Text> sentences;

    private int index = 0;
    private MajorController major;
    public bool useMajor = false;
    public OotiEvent eventToSend;
    public float timer = 0;
    public float deltaTime = 0;

    // Use this for initialization
    void Awake()
    {
        if (useMajor)
            major = GetComponentInParent<MajorController>();

        
    }

    void Start()
    {
        sentences[0].gameObject.SetActive(true);
        deltaTime = Time.time + 1f;
    }

    public void OnMouseUpAsButton()
    {
        if (Time.time < deltaTime)
            return;

        FMODUnity.RuntimeManager.PlayOneShot("event:/boutok", transform.position);

        deltaTime = Time.time + 1f;
        if (index <= sentences.Count)
        {
            index++;
            if (index < sentences.Count)
            {
                sentences[index - 1].gameObject.SetActive(false);
                sentences[index].gameObject.SetActive(true);
            }
        }

        if (index == sentences.Count)
        {
            SetTrigger();
        }
    }


    void OnEnable()
    {
        foreach (Text txt in sentences)
            txt.gameObject.SetActive(false);

        sentences[0].gameObject.SetActive(true);
    }


    void SetTrigger()
    {
        if (useMajor)
            major.SetNextStep();

        if (eventToSend != OotiEvent.none)
            MessageDispatcher.SendMessage(eventToSend.ToString(), timer);
        Destroy(gameObject);
    }

}