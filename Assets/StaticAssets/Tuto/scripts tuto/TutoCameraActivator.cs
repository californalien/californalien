﻿using UnityEngine;
using System.Collections;
using com.ootii.Messages;

public class TutoCameraActivator : MonoBehaviour {

    public OotiEvent activateEvent;
    public OotiEvent desactivateSendEvent;
    public float timerBeforeDesactivate;

	// Use this for initialization
	void Awake ()
    {
        MessageDispatcher.AddListener(activateEvent.ToString(), Activate);
	}

    void OnDestroy()
    {
        MessageDispatcher.RemoveListener(activateEvent.ToString(), Activate);
    }
	
    void Activate(IMessage mess)
    {
        GetComponent<CameraDrag>().enabled = true;
        Invoke("Desactivate", timerBeforeDesactivate);
    }

    void Desactivate()
    {
        GetComponent<CameraDrag>().enabled = false;
        MessageDispatcher.SendMessage(desactivateSendEvent.ToString());
    }
}
