﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class SplashScreenLoader : MonoBehaviour 
{

	public string sceneToLoad;

	public void LoadScene (){
		SceneManager.LoadScene (sceneToLoad);

	}


}
