﻿using UnityEngine;
using System.Collections;

public class LeverController : MonoBehaviour {

    bool isActive = false;
    public void Activate()
    {
        if(isActive == false)
        {

            SLML_Manager.Instance.GetComponent<MenuMusic>().OnDiscoTime();
            isActive = true;
        }
        else
        {
            SLML_Manager.Instance.GetComponent<MenuMusic>().OnEndDiscoTime();
            isActive = false;
        }
    }
}
